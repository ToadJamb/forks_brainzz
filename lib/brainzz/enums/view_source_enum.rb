module Brainzz
  module ViewSourceEnum
    extend self

    ADVERTISING      =  1
    ANNOTATION       =  2
    EXT_URL          =  3
    NO_LINK_EMBEDDED =  4
    NO_LINK_OTHER    =  5
    PLAYLIST         =  6
    PROMOTED         =  7
    RELATED_VIDEO    =  8
    SUBSCRIBER       =  9
    YT_CHANNEL       = 10
    YT_OTHER_PAGE    = 11
    YT_SEARCH        = 12
    EXT_APP          = 13
    INFO_CARD        = 14

    UNKNOWN          = -1

    SOURCES = {
      'advertising'      => ADVERTISING,
      'annotation'       => ANNOTATION,
      'ext_url'          => EXT_URL,
      'no_link_embedded' => NO_LINK_EMBEDDED,
      'no_link_other'    => NO_LINK_OTHER,
      'playlist'         => PLAYLIST,
      'promoted'         => PROMOTED,
      'related_video'    => RELATED_VIDEO,
      'subscriber'       => SUBSCRIBER,
      'yt_channel'       => YT_CHANNEL,
      'yt_other_page'    => YT_OTHER_PAGE,
      'yt_search'        => YT_SEARCH,
      'ext_app'          => EXT_APP,
      'info_card'        => INFO_CARD,
    }

    def key_for(value)
      constants.each do |constant|
        return constant.to_s.downcase if const_get(constant) == value
      end
      nil
    end
  end
end
