module Brainzz
  class VideoStatsResponse < Reverb::Response
    class << self
      def model
        @model
      end

      private

      def make_with_model(model)
        @model = model
      end
    end

    private

    def on_success
      self.data = []
      if body['rows']
        keys = column_headers
        body['rows'].each do |row|
          self.data << self.class.model.new({
            :data => row,
            :keys => keys,
          })
        end
      end
    end

    def column_headers
      column_headers = {}

      body['columnHeaders'].each_with_index do |header, index|
        column_headers[header['name']] = index
      end

      column_headers
    end
  end
end
