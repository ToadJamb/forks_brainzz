module Brainzz
  class AnnotationsClickableResponse < VideoStatsResponse
    make_with_model AnnotationsClickableCount
  end
end
