module Brainzz
  class PlaylistItemsResponse < ::Reverb::Response
    def on_success
      self.data = PlaylistItemsWrapper.new(body)
    end
  end
end
