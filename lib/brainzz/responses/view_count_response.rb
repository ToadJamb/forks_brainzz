module Brainzz
  class ViewCountResponse < VideoStatsResponse
    make_with_model ViewCount
  end
end
