module Brainzz
  class ShareCountResponse < VideoStatsResponse
    make_with_model ShareCount
  end
end
