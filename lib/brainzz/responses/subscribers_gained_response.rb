module Brainzz
  class SubscribersGainedResponse < VideoStatsResponse
    make_with_model SubscribersGainedCount
  end
end
