module Brainzz
  class VideoDetailsResponse < ::Reverb::Response
    def on_success
      self.data = {}

      items = body['items'] || []
      items.each do |item|
        video = Video.new(item)
        self.data[video.id] = video
      end
    end
  end
end
