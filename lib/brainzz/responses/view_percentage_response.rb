module Brainzz
  class ViewPercentageResponse < VideoStatsResponse
    make_with_model ViewPercentage
  end
end
