module Brainzz
  class AnnotationsClickedResponse < VideoStatsResponse
    make_with_model AnnotationsClickedCount
  end
end
