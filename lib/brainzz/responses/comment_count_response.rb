module Brainzz
  class CommentCountResponse < VideoStatsResponse
    make_with_model CommentCount
  end
end
