module Brainzz
  class SubscribersLostResponse < VideoStatsResponse
    make_with_model SubscribersLostCount
  end
end
