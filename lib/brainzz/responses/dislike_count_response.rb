module Brainzz
  class DislikeCountResponse < VideoStatsResponse
    make_with_model DislikeCount
  end
end
