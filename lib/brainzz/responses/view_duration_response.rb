module Brainzz
  class ViewDurationResponse < VideoStatsResponse
    make_with_model ViewDuration
  end
end
