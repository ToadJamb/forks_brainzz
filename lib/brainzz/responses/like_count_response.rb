module Brainzz
  class LikeCountResponse < VideoStatsResponse
    make_with_model LikeCount
  end
end
