module Brainzz
  module AccessTokenService
    extend self

    @@mutex = Mutex.new

    def retrieve_token(refresh_token)
      @@mutex.synchronize do
        if !defined?(@token) or @token.expired?
          response = Toke.retrieve_token(params(refresh_token))
          @token = response.data if response.success?
        end

        @token.token if defined?(@token)
      end
    end

    private

    def params(refresh_token)
      {
        :refresh_token => refresh_token,
        :client_id     => ENV['BRAINZZ_CLIENT_ID'],
        :client_secret => ENV['BRAINZZ_CLIENT_SECRET'],
      }
    end
  end
end
