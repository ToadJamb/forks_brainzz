module Brainzz
  module Core
    extend self

    def playlist_items_for(playlist_id, response = nil)
      PlaylistItemsCommand.execute playlist_id, response
    end

    def video_details_for(video_ids)
      VideoDetailsCommand.execute video_ids
    end

    def views_by_day_for(video_id, start_date, end_date)
      ViewCountByDayCommand.execute video_id, start_date, end_date
    end

    def view_totals_for(video_id, start_date, end_date)
      ViewTotalsCommand.execute video_id, start_date, end_date
    end

    def likes_by_day_for(video_id, start_date, end_date)
      LikeCountByDayCommand.execute video_id, start_date, end_date
    end

    def like_totals_for(video_id, start_date, end_date)
      LikeTotalsCommand.execute video_id, start_date, end_date
    end

    def shares_by_day_for(video_id, start_date, end_date)
      ShareCountByDayCommand.execute video_id, start_date, end_date
    end

    def share_totals_for(video_id, start_date, end_date)
      ShareTotalsCommand.execute video_id, start_date, end_date
    end

    def dislikes_by_day_for(video_id, start_date, end_date)
      DislikeCountByDayCommand.execute video_id, start_date, end_date
    end

    def dislike_totals_for(video_id, start_date, end_date)
      DislikeTotalsCommand.execute video_id, start_date, end_date
    end

    def comments_by_day_for(video_id, start_date, end_date)
      CommentCountByDayCommand.execute video_id, start_date, end_date
    end

    def comment_totals_for(video_id, start_date, end_date)
      CommentTotalsCommand.execute video_id, start_date, end_date
    end

    def subscribers_gained_by_day_for(video_id, start_date, end_date)
      SubscribersGainedByDayCommand.execute video_id, start_date, end_date
    end

    def subscribers_gained_totals_for(video_id, start_date, end_date)
      SubscribersGainedTotalsCommand.execute video_id, start_date, end_date
    end

    def subscribers_lost_by_day_for(video_id, start_date, end_date)
      SubscribersLostByDayCommand.execute video_id, start_date, end_date
    end

    def subscribers_lost_totals_for(video_id, start_date, end_date)
      SubscribersLostTotalsCommand.execute video_id, start_date, end_date
    end

    def annotations_clickable_by_day_for(video_id, start_date, end_date)
      AnnotationsClickableByDayCommand.execute video_id, start_date, end_date
    end

    def annotations_clickable_totals_for(video_id, start_date, end_date)
      AnnotationsClickableTotalsCommand.execute video_id, start_date, end_date
    end

    def annotations_clicked_by_day_for(video_id, start_date, end_date)
      AnnotationsClickedByDayCommand.execute video_id, start_date, end_date
    end

    def annotations_clicked_totals_for(video_id, start_date, end_date)
      AnnotationsClickedTotalsCommand.execute video_id, start_date, end_date
    end

    def view_percentages_by_day_for(video_id, start_date, end_date)
      ViewPercentagesByDayCommand.execute video_id, start_date, end_date
    end

    def view_percentage_totals_for(video_id, start_date, end_date)
      ViewPercentageTotalsCommand.execute video_id, start_date, end_date
    end

    def view_durations_by_day_for(video_id, start_date, end_date)
      ViewDurationsByDayCommand.execute video_id, start_date, end_date
    end

    def view_duration_totals_for(video_id, start_date, end_date)
      ViewDurationTotalsCommand.execute video_id, start_date, end_date
    end
  end
end
