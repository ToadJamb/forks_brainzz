module Brainzz
  class DataCommand < BaseCommand
    class << self
      def params(parameters = nil)
        super.merge({
          :key => api_key,
        })
      end

      def base_url
        'https://www.googleapis.com/youtube/v3'
      end

      def api_key
        ENV['BRAINZZ_GOOGLE_API_KEY']
      end
    end
  end
end
