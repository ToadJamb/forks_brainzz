module Brainzz
  class BaseCommand
    class << self
      private

      def get(parameters)
        connection.get endpoint, params(parameters), headers
      end

      def connection
        Faraday.new(:url => base_url)
      end

      def params(parameters = nil)
        {}
      end

      def headers
        {
          'Content-Type' => 'application/json',
        }
      end

      def url(parameters)
        connection.build_url endpoint, params(parameters)
      end
    end
  end
end
