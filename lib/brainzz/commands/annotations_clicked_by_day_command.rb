module Brainzz
  class AnnotationsClickedByDayCommand < AnnotationsClickedTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response AnnotationsClickedResponse
  end
end
