module Brainzz
  class AnnotationsClickedTotalsCommand < VideoStatsCommand
    make_with_response AnnotationsClickedResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'annotationClicks',
        })
      end
    end
  end
end
