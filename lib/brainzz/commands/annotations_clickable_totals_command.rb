module Brainzz
  class AnnotationsClickableTotalsCommand < VideoStatsCommand
    make_with_response AnnotationsClickableResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'annotationClickableImpressions',
        })
      end
    end
  end
end
