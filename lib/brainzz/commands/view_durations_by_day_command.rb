module Brainzz
  class ViewDurationsByDayCommand < ViewDurationTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response ViewDurationResponse
  end
end
