module Brainzz
  class SubscribersGainedByDayCommand < SubscribersGainedTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response SubscribersGainedResponse
  end
end
