module Brainzz
  class VideoDetailsCommand < DataCommand
    class << self
      def execute(video_ids)
        video_details_params = VideoDetailsParams.new(video_ids)

        if video_details_params.valid?
          response = get(video_details_params)
          VideoDetailsResponse.new response
        else
          VideoDetailsResponse.new
        end
      end

      private

      def endpoint
        'videos'
      end

      def params(video_details_params)
        super.merge({
          'id'   => video_details_params.video_ids.join(','),
          'part' => 'snippet',
        })
      end
    end
  end
end
