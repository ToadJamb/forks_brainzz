module Brainzz
  class AnalyticsCommand < BaseCommand
    class << self
      private

      def base_url
        'https://www.googleapis.com/youtube/analytics/v1'
      end

      def endpoint
        'reports'
      end

      def headers
        super.merge({
          'Authorization' => "Bearer #{access_token}",
        })
      end

      def access_token
        AccessTokenService.retrieve_token refresh_token
      end

      def refresh_token
        ENV['BRAINZZ_REFRESH_TOKEN']
      end

      def params(analytics_params)
        super.merge({
          'ids'        => "contentOwner==#{content_owner}",
          'start-date' => analytics_params.start_date,
          'end-date'   => analytics_params.end_date,
        })
      end

      def content_owner
        ENV['BRAINZZ_CONTENT_OWNER']
      end
    end
  end
end
