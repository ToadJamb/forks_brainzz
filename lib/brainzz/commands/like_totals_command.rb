module Brainzz
  class LikeTotalsCommand < VideoStatsCommand
    make_with_response LikeCountResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'likes',
        })
      end
    end
  end
end
