module Brainzz
  class ViewCountByDayCommand < ViewTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response ViewCountResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'dimensions' => 'day,insightTrafficSourceType',
        })
      end
    end
  end
end
