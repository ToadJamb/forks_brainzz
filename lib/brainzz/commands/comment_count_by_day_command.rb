module Brainzz
  class CommentCountByDayCommand < CommentTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response CommentCountResponse
  end
end
