module Brainzz
  class SubscribersLostTotalsCommand < VideoStatsCommand
    make_with_response SubscribersLostResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'subscribersLost',
        })
      end
    end
  end
end
