module Brainzz
  class ViewTotalsCommand < VideoStatsCommand
    make_with_response ViewCountResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics'    => 'views',
          'dimensions' => 'insightTrafficSourceType',
        })
      end
    end
  end
end
