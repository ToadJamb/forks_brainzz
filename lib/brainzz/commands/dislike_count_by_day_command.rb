module Brainzz
  class DislikeCountByDayCommand < DislikeTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response DislikeCountResponse
  end
end
