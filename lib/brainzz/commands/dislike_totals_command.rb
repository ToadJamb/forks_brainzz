module Brainzz
  class DislikeTotalsCommand < VideoStatsCommand
    make_with_response DislikeCountResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'dislikes',
        })
      end
    end
  end
end
