module Brainzz
  class ViewPercentagesByDayCommand < ViewPercentageTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response ViewPercentageResponse
  end
end
