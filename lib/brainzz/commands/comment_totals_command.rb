module Brainzz
  class CommentTotalsCommand < VideoStatsCommand
    make_with_response CommentCountResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'comments',
        })
      end
    end
  end
end
