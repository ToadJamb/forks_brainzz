module Brainzz
  class SubscribersGainedTotalsCommand < VideoStatsCommand
    make_with_response SubscribersGainedResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'subscribersGained',
        })
      end
    end
  end
end
