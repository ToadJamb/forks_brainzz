module Brainzz
  class ViewPercentageTotalsCommand < VideoStatsCommand
    make_with_response ViewPercentageResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'averageViewPercentage',
        })
      end
    end
  end
end
