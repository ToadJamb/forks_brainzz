module Brainzz
  class SubscribersLostByDayCommand < SubscribersLostTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response SubscribersLostResponse
  end
end
