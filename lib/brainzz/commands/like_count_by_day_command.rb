module Brainzz
  class LikeCountByDayCommand < LikeTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response LikeCountResponse
  end
end
