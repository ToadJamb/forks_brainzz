module Brainzz
  class VideoStatsCommand < AnalyticsCommand
    class << self
      def execute(video_id, start_date, end_date)
        video_stats_params = VideoStatsParams.new ({
          :video_id   => video_id,
          :start_date => start_date,
          :end_date   => end_date,
        })

        if video_stats_params.valid?
          response.new get(video_stats_params)
        else
          response.new
        end
      end

      private

      def response
        @response
      end

      def make_with_response(response)
        @response = response
      end

      def params(video_stats_params)
        super.merge({
          'filters' => "video==#{video_stats_params.video_id}",
        })
      end
    end
  end
end
