module Brainzz
  class AnnotationsClickableByDayCommand < AnnotationsClickableTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response AnnotationsClickableResponse
  end
end
