module Brainzz
  class ViewDurationTotalsCommand < VideoStatsCommand
    make_with_response ViewDurationResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'averageViewDuration',
        })
      end
    end
  end
end
