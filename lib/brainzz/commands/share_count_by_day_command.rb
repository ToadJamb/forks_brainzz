module Brainzz
  class ShareCountByDayCommand < ShareTotalsCommand
    extend VideoStatsByDayCommand

    make_with_response ShareCountResponse
  end
end
