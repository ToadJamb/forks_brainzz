module Brainzz
  class PlaylistItemsCommand < DataCommand
    class << self
      def execute(playlist_id, response)
        playlist_items_params = PlaylistItemsParams.new(playlist_id, response)

        if playlist_items_params.valid?
          response = get(playlist_items_params)
          PlaylistItemsResponse.new response
        else
          PlaylistItemsResponse.new
        end
      end

      private

      def endpoint
        'playlistItems'
      end

      def params(playlist_items_params)
        args = {
          'playlistId' => playlist_items_params.playlist_id,
          'part'       => 'contentDetails',
          'maxResults' => 50,
        }

        if playlist_items_params.next_page_token
          args.merge!({
            'pageToken' => playlist_items_params.next_page_token
          })
        end

        super.merge args
      end
    end
  end
end
