module Brainzz
  module VideoStatsByDayCommand
    private

    def params(video_stats_params)
      super.merge({
        'dimensions' => 'day',
        'sort'       => 'day',
      })
    end
  end
end
