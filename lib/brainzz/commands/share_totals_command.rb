module Brainzz
  class ShareTotalsCommand < VideoStatsCommand
    make_with_response ShareCountResponse

    class << self
      private

      def params(video_stats_params)
        super.merge({
          'metrics' => 'shares',
        })
      end
    end
  end
end
