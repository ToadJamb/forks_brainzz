FactoryGirl.define do
  factory :brainzz_playlist_item, :class => Brainzz::PlaylistItem do
    id { FactoryGirl.generate :brainzz_playlist_item_id }
    video { FactoryGirl.build :brainzz_video }
  end
end
