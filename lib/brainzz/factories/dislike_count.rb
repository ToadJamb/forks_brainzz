FactoryGirl.define do
  factory :brainzz_dislike_count, :class => Brainzz::DislikeCount do
    dislikes { rand(1_000_000) + 1 }
    day      { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_dislike_count do
      day nil
    end

    initialize_with do
      Brainzz::DislikeCount.new({})
    end
  end
end
