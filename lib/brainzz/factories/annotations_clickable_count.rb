FactoryGirl.define do
  factory :brainzz_annotations_clickable_count,
    :class => Brainzz::AnnotationsClickableCount do
    annotations_clickable { rand(1_000_000) + 1 }
    day { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_annotations_clickable_count do
      day nil
    end

    initialize_with do
      Brainzz::AnnotationsClickableCount.new({})
    end
  end
end
