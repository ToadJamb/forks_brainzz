FactoryGirl.define do
  factory :brainzz_view_count, :class => Brainzz::ViewCount do
    views  { rand(1_000_000) + 1 }
    day    { Time.parse (DateTime.now - rand(100)).strftime('%F') }
    source { generate :brainzz_view_count_source }

    factory :brainzz_aggregate_view_count do
      day nil
    end

    Brainzz::ViewSourceEnum::SOURCES.each do |key, value|
      factory "brainzz_#{key}_view_count".to_sym,
          :class => Brainzz::ViewCount do
        source value
      end

      factory "brainzz_#{key}_aggregate_view_count".to_sym,
          :class => Brainzz::ViewCount do
        source value
        day nil
      end
    end

    factory :brainzz_unknown_view_count do
      source nil
    end

    factory :brainzz_unknown_aggregate_view_count do
      source Brainzz::ViewSourceEnum::UNKNOWN
      day nil
    end

    initialize_with do
      Brainzz::ViewCount.new({})
    end
  end
end
