FactoryGirl.define do
  factory :brainzz_like_count, :class => Brainzz::LikeCount do
    likes { rand(1_000_000) + 1 }
    day   { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_like_count do
      day nil
    end

    initialize_with do
      Brainzz::LikeCount.new({})
    end
  end
end
