FactoryGirl.define do
  factory :brainzz_video, :class => Brainzz::Video do
    id { FactoryGirl.generate :brainzz_video_id }

    factory :brainzz_published_video do
      title        { FactoryGirl.generate :brainzz_video_title }
      published_at { Time.now - 60 * 60 * 24 * rand(1_500) }
    end
  end
end
