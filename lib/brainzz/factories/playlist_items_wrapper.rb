FactoryGirl.define do
  factory :brainzz_playlist_items_wrapper,
      :class => Brainzz::PlaylistItemsWrapper do
    next_page_token { FactoryGirl.generate :brainzz_next_page_token }
    playlist_items { FactoryGirl.build_list :brainzz_playlist_item, 3 }
  end
end
