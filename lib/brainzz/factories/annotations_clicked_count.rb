FactoryGirl.define do
  factory :brainzz_annotations_clicked_count,
    :class => Brainzz::AnnotationsClickedCount do
    annotations_clicked { rand(1_000_000) + 1 }
    day { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_annotations_clicked_count do
      day nil
    end

    initialize_with do
      Brainzz::AnnotationsClickedCount.new({})
    end
  end
end
