FactoryGirl.define do
  factory :brainzz_subscribers_gained_count,
    :class => Brainzz::SubscribersGainedCount do
    subscribers_gained { rand(1_000_000) + 1 }
    day                { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_subscribers_gained_count do
      day nil
    end

    initialize_with do
      Brainzz::SubscribersGainedCount.new({})
    end
  end
end
