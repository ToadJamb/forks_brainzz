FactoryGirl.define do
  factory :brainzz_view_percentage, :class => Brainzz::ViewPercentage do
    percentage { rand(1_000_000) / 10000.0 }
    day { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_view_percentage do
      day nil
    end

    initialize_with do
      Brainzz::ViewPercentage.new({})
    end
  end
end
