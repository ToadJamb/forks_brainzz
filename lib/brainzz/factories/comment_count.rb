FactoryGirl.define do
  factory :brainzz_comment_count, :class => Brainzz::CommentCount do
    comments { rand(1_000_000) + 1 }
    day { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_comment_count do
      day nil
    end

    initialize_with do
      Brainzz::CommentCount.new({})
    end
  end
end
