FactoryGirl.define do
  factory :brainzz_subscribers_lost_count,
    :class => Brainzz::SubscribersLostCount do
    subscribers_lost { rand(1_000_000) + 1 }
    day              { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_subscribers_lost_count do
      day nil
    end

    initialize_with do
      Brainzz::SubscribersLostCount.new({})
    end
  end
end
