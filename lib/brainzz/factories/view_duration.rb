FactoryGirl.define do
  factory :brainzz_view_duration, :class => Brainzz::ViewDuration do
    duration { rand(1_000_000) + 1 }
    day { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_view_duration do
      day nil
    end

    initialize_with do
      Brainzz::ViewDuration.new({})
    end
  end
end
