FactoryGirl.define do
  factory :brainzz_share_count, :class => Brainzz::ShareCount do
    shares { rand(1_000_000) + 1 }
    day    { Time.parse (DateTime.now - rand(100)).strftime('%F') }

    factory :brainzz_aggregate_share_count do
      day nil
    end

    initialize_with do
      Brainzz::ShareCount.new({})
    end
  end
end
