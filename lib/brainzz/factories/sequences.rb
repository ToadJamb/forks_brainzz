FactoryGirl.define do
  sequence(:brainzz_fake_string) { ['foo', 'bar', 'baz'].sample }

  sequence(:brainzz_video_id)    { |n| "video-id-#{n}" }
  sequence(:brainzz_video_title) do
    "video-title-#{FactoryGirl.generate(:brainzz_fake_string)}"
  end

  sequence(:brainzz_playlist_item_id) { |n| "playlist-item-id-#{n}" }

  sequence(:brainzz_next_page_token) { |n| "page-token-#{n}" }

  sequence(:brainzz_view_count_source) do |n|
    n % Brainzz::ViewSourceEnum::SOURCES.count + 1
  end
end
