module Brainzz
  class VideoStatsParams < AnalyticsParams
    attr_reader :video_id

    def initialize(params)
      super
      @video_id = normalize(params[:video_id])
    end

    def valid?
      !!(super && video_id)
    end
  end
end
