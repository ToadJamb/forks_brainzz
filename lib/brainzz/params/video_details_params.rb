module Brainzz
  class VideoDetailsParams
    attr_reader :video_ids

    def initialize(video_ids)
      @video_ids = video_ids
    end

    def valid?
      !!(@video_ids && !@video_ids.empty?)
    end
  end
end
