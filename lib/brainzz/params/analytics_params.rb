module Brainzz
  class AnalyticsParams < BaseParams
    attr_reader :start_date
    attr_reader :end_date

    def initialize(params)
      @start_date = normalize_date(params[:start_date])
      @end_date   = normalize_date(params[:end_date])
    end

    def valid?
      !!(start_date && end_date)
    end
  end
end
