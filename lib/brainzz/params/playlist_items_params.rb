module Brainzz
  class PlaylistItemsParams < BaseParams
    attr_reader :playlist_id, :next_page_token

    def initialize(playlist_id, response)
      @playlist_id     = normalize(playlist_id)
      @next_page_token = page_token_for(response)
    end

    def valid?
      !!playlist_id
    end

    private

    def page_token_for(response)
      response && response.data && response.data.next_page_token
    end
  end
end
