module Brainzz
  class BaseParams
    private

    def normalize(value)
      value = nil if value && value.strip == ''
      value
    end

    def normalize_date(date)
      date.strftime('%F') if date.respond_to?(:strftime)
    end
  end
end
