module Brainzz
  class VideoStat < BaseModel
    attr_accessor :day

    def initialize(hash)
      @hash = hash

      @day  = transform_date(day_data)
    end

    private

    def data
      @hash['data'] || @hash[:data]
    end

    def keys
      @hash['keys'] || @hash[:keys]
    end

    def day_data
      data && keys['day'] && data[keys['day']]
    end

    def normalize_stat(stat)
      stat.to_i if stat
    end
  end
end
