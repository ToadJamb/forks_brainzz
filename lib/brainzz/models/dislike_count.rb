module Brainzz
  class DislikeCount < VideoStat
    attr_accessor :dislikes

    def initialize(hash)
      super

      @dislikes = normalize_stat(dislikes_data)
    end

    private

    def dislikes_data
      data && keys['dislikes'] && data[keys['dislikes']]
    end
  end
end
