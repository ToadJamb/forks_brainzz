module Brainzz
  class SubscribersGainedCount < VideoStat
    attr_accessor :subscribers_gained

    def initialize(hash)
      super

      @subscribers_gained = normalize_stat(subscribers_gained_data)
    end

    private

    def subscribers_gained_data
      data && keys['subscribersGained'] && data[keys['subscribersGained']]
    end
  end
end
