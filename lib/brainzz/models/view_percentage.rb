module Brainzz
  class ViewPercentage < VideoStat
    attr_accessor :percentage

    def initialize(hash)
      super

      @percentage = normalize_stat(percentage_data)
    end

    private

    def normalize_stat(stat)
      stat.to_f if stat
    end

    def percentage_data
      data && keys['averageViewPercentage'] &&
        data[keys['averageViewPercentage']]
    end
  end
end
