module Brainzz
  class LikeCount < VideoStat
    attr_accessor :likes

    def initialize(hash)
      super

      @likes = normalize_stat(likes_data)
    end

    private

    def likes_data
      data && keys['likes'] && data[keys['likes']]
    end
  end
end
