module Brainzz
  class PlaylistItem
    attr_accessor :id, :video

    def initialize(hash = {})
      @hash = hash

      @id = hash['id']
      @video = Video.new(video_hash)
    end

    private

    def video_hash
      @hash['contentDetails']
    end
  end
end
