module Brainzz
  class PlaylistItemsWrapper
    attr_accessor :next_page_token, :playlist_items

    def initialize(playlist_data = {})
      @playlist_items = []
      items = playlist_data['items'] || []
      items.each do |playlist_item|
        @playlist_items << PlaylistItem.new(playlist_item)
      end

      @next_page_token = playlist_data['nextPageToken']
    end

    def last_page?
      !next_page_token
    end
  end
end
