module Brainzz
  class Video < BaseModel
    attr_accessor :id, :title, :published_at

    def initialize(hash = {})
      @hash = hash || {}

      @id           = transform(hash_id)
      @title        = transform(hash_title)
      @published_at = transform_date(hash_published_at)
    end

    private

    def snippet
      @hash['snippet'] || {}
    end

    def hash_id
      @hash['videoId'] || @hash['id']
    end

    def hash_title
      snippet['title']
    end

    def hash_published_at
      snippet['publishedAt']
    end
  end
end
