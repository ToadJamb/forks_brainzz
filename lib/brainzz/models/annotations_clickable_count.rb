module Brainzz
  class AnnotationsClickableCount < VideoStat
    attr_accessor :annotations_clickable

    def initialize(hash)
      super

      @annotations_clickable = normalize_stat(annotations_clickable_data)
    end

    private

    def annotations_clickable_data
      data && keys['annotationClickableImpressions'] &&
        data[keys['annotationClickableImpressions']]
    end
  end
end
