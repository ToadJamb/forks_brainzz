module Brainzz
  class AnnotationsClickedCount < VideoStat
    attr_accessor :annotations_clicked

    def initialize(hash)
      super

      @annotations_clicked = normalize_stat(annotations_clicked_data)
    end

    private

    def annotations_clicked_data
      data && keys['annotationClicks'] && data[keys['annotationClicks']]
    end
  end
end
