module Brainzz
  class SubscribersLostCount < VideoStat
    attr_accessor :subscribers_lost

    def initialize(hash)
      super

      @subscribers_lost = normalize_stat(subscribers_lost_data)
    end

    private

    def subscribers_lost_data
      data && keys['subscribersLost'] && data[keys['subscribersLost']]
    end
  end
end
