module Brainzz
  class ViewDuration < VideoStat
    attr_accessor :duration

    def initialize(hash)
      super

      @duration = normalize_stat(duration_data)
    end

    private

    def duration_data
      data && keys['averageViewDuration'] &&
        data[keys['averageViewDuration']]
    end
  end
end
