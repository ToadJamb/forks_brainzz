module Brainzz
  class ShareCount < VideoStat
    attr_accessor :shares

    def initialize(hash)
      super

      @shares = normalize_stat(shares_data)
    end

    private

    def shares_data
      data && keys['shares'] && data[keys['shares']]
    end
  end
end
