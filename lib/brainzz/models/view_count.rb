module Brainzz
  class ViewCount < VideoStat
    attr_accessor :views, :source

    def initialize(hash)
      super

      @views  = normalize_stat(views_data)
      @source = source_value(source_data)
    end

    private

    def views_data
      data && keys['views'] && data[keys['views']]
    end

    def source_data
      data && keys['insightTrafficSourceType'] &&
        data[keys['insightTrafficSourceType']]
    end

    def source_value(source)
      return if source.nil?
      source = '' unless source.is_a?(String)
      Brainzz::ViewSourceEnum::SOURCES[source.downcase]
    end
  end
end
