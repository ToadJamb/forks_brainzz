module Brainzz
  class CommentCount < VideoStat
    attr_accessor :comments

    def initialize(hash)
      super

      @comments = normalize_stat(comment_data)
    end

    private

    def comment_data
      data && keys['comments'] && data[keys['comments']]
    end
  end
end
