module Brainzz
  class BaseModel
    private

    def transform(value)
      case value
      when String
        value = transform_string(value)
      end
      value
    end

    def transform_string(value)
      value = value.strip
      value = nil if value == ''
      value
    end

    def transform_date(value)
      value = transform(value)
      return nil unless value.is_a?(String)
      Time.parse value
    end
  end
end
