Gem::Specification.new do |spec|
  spec.name          = 'brainzz'
  spec.version       = '0.0.9'
  spec.authors      << 'Travis Herrick'
  spec.authors      << 'Joshua Book'
  spec.authors      << 'Haskel Ash'
  spec.email         = ['travish@awesomenesstv.com']
  spec.summary       = 'YouTube API interface'
  spec.description   = '
    Provides an interface to the YouTube API.
  '.strip

  spec.homepage      = 'https://github.com/awesomenesstv/brainzz'
  spec.license       = 'LGPLv3'
  spec.files         = Dir['lib/**/*.rb', 'license/*']

  spec.extra_rdoc_files = [
    'README.md',
    'license/gplv3.md',
    'license/lgplv3.md',
  ]

  spec.add_dependency 'faraday', '~> 0'
  spec.add_dependency 'reverb',  '~> 0'
  spec.add_dependency 'toke',    '~> 0'

  spec.add_development_dependency 'gems',         '~> 0'
  spec.add_development_dependency 'rake_tasks',   '~> 4'
  spec.add_development_dependency 'cane',         '~> 2'
  spec.add_development_dependency 'rspec',        '~> 3'
  spec.add_development_dependency 'vcr',          '~> 2'
  spec.add_development_dependency 'webmock',      '~> 1'
  spec.add_development_dependency 'dotenv',       '~> 2'
  spec.add_development_dependency 'factory_girl', '~> 4'
end
