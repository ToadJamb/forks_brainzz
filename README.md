Brainzz
=======

[![Build Status](https://semaphoreci.com/api/v1/projects/6bcf66fa-7d8c-4027-93ae-13b6bb75325f/346455/badge.png)](https://semaphoreci.com/awesomenesstv/brainzz)

Brainzz explains why youtoobers' brainzz are turned to mush.
... by extracting data from the YouTube API.


Setup
-----

	$ bundle install


Run Specs
---------

	$ bundle exec rake


### Regenerating VCR Cassettes

Check `config/dotenv/test.env` for the required keys.
The values in this file are required environment variables
when consuming this gem.

Environment Variables
---------------------
Brainzz requires the use of multiple environment variables:
- BRAINZZ_GOOGLE_API_KEY
- BRAINZZ_CLIENT_ID
- BRAINZZ_CLIENT_SECRET
- BRAINZZ_REFRESH_TOKEN
- BRAINZZ_CONTENT_OWNER:
 - the Google account for the CMS that owns the YouTube channel data being sought

Place values in `config/dotenv/integration.env`.

### Attaining BRAINZZ_GOOGLE_API_KEY

Follow the instructions [here][atv-wiki-google-api-key]
to find the required values.

### Attaining BRAINZZ_REFRESH_TOKEN
An OAUTH refresh token for the BRAINZZ_CONTENT_OWNER Google account
uses BRAINZZ_CLIENT_SECRET and BRAINZZ_CLIENT_SECRET

`BRAINZZ_CLIENT_ID` and `BRAINZZ_CLIENT_SECRET`.
- Go to the [Google OAuth Playground][google-oauth-playground]
- Log in with the Google account for BRAINZZ_CONTENT_OWNER
- Click on the gear (upper-right)
- Click check box to "Use your own OAuth credentials"
- Fill in BRAINZZ_CLIENT_ID and BRAINZZ_CLIENT_SECRET
- In Step 1 (Select & Authorize APIs) scroll down to YouTube Analytics API v1
- Click the last item "yt-analytics.readonly"
- Click blue 'Authorize APIs' button
- Exchange code for tokens
- Grab value for the Refresh Token and store

Interface
---------
All public endpoints are exposed in Brainzz::Core.

Every response from the public API is wrapped in a `Response` object
that will always have the same interface regardless of request.
The `Response#data` attribute will be an object specific to the data requested.


### playlist_items_for

On first invocation, only a playlist id is required:

```
playlist_id = 'my-playlist-id'
Brainzz.playlist_items_for playlist_id
```

This data is paginated, so if there are more than 50 results,
subsequent calls will need to pass in the response from the previous request.
In order to retrieve all playlist items,
the `last_page?` attribute will need to be checked as well.

```
playlist_id = 'my-playlist-id'
response = nil
while response.nil? || !response.data.last_page?
  response = Brainzz.playlist_items_for(playlist_id, response)
end
```


### video_details_for

Accepts an array of video ids.
The maximum length of the array is determined by YouTube.
It is currently limited to 50 videos.

This endpoint returns a hash with video ids as keys
and video objects as the values.


### views_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::ViewCount` objects ordered by date in ascending order.

`#source` will be an integer (or nil) value
corresponding to the following YouTube referrals:

	ADVERTISING      =  1
	ANNOTATION       =  2
	EXT_URL          =  3
	NO_LINK_EMBEDDED =  4
	NO_LINK_OTHER    =  5
	PLAYLIST         =  6
	PROMOTED         =  7
	RELATED_VIDEO    =  8
	SUBSCRIBER       =  9
	YT_CHANNEL       = 10
	YT_OTHER_PAGE    = 11
	YT_SEARCH        = 12

The constants are defined in `Brainzz::ViewCountEnum`.

### view_totals_for

#### Parameters
Same as views_by_day_for.

#### Result
Same as views_by_day_for except date values are nil.

Views for date range are aggregated per source type.

### likes_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::LikeCount` objects ordered by date in ascending order.

### like_totals_for

#### Parameters
Same as likes_by_day_for.

#### Result
Same as likes_by_day_for except date values are nil.

Likes for date range are aggregated.

### shares_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::ShareCount` objects ordered by date in ascending order.

### share_totals_for

#### Parameters
Same as shares_by_day_for.

#### Result
Same as shares_by_day_for except date values are nil.

Shares for date range are aggregated.

### dislikes_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::DislikeCount` objects ordered by date in ascending order.

### dislike_totals_for

#### Parameters
Same as dislikes_by_day_for.

#### Result
Same as dislikes_by_day_for except date values are nil.

Dislikes for date range are aggregated.

### comments_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::CommentCount` objects ordered by date in ascending order.

### comment_totals_for

#### Parameters
Same as comments_by_day_for.

#### Result
Same as comments_by_day_for except date values are nil.

Comments for date range are aggregated.

### subscribers_gained_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::SubscribersGainedCount` objects ordered by date in ascending order.

### subscribers_gained_totals_for

#### Parameters
Same as subscribers_gained_by_day_for.

#### Result
Same as subscribers_gained_by_day_for except date values are nil.

SubscribersGained for date range are aggregated.

### subscribers_lost_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::SubscribersLostCount` objects ordered by date in ascending order.

### subscribers_lost_totals_for

#### Parameters
Same as subscribers_lost_by_day_for.

#### Result
Same as subscribers_lost_by_day_for except date values are nil.

SubscribersLost for date range are aggregated.

### annotations_clickable_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::AnnotationsClickableCount` objects ordered by date in ascending order.

### annotations_clickable_totals_for

#### Parameters
Same as annotations_clickable_by_day_for.

#### Result
Same as annotations_clickable_by_day_for except date values are nil.

AnnotationsClickable for date range are aggregated.

### annotations_clicked_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::AnnotationsClickedCount` objects ordered by date in ascending order.

### annotations_clicked_totals_for

#### Parameters
Same as annotations_clicked_by_day_for.

#### Result
Same as annotations_clicked_by_day_for except date values are nil.

AnnotationsClicked for date range are aggregated.

### view_percentages_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::ViewPercentage` objects ordered by date in ascending order.

### view_percentage_totals_for

#### Parameters
Same as view_percentages_by_day_for.

#### Result
Same as view_percentages_by_day_for except date values are nil.

ViewPercentages for date range are aggregated.

### view_durations_by_day_for

#### Parameters

* video id      (String)
* start date    (DateTime)
* end date      (DateTime)

#### Result

An array of `Brainzz::ViewDuration` objects ordered by date in ascending order.

### view_duration_totals_for

#### Parameters
Same as view_durations_by_day_for.

#### Result
Same as view_durations_by_day_for except date values are nil.

ViewDurations for date range are aggregated.

Factories
---------

Brainzz includes FactoryGirl factories for your convenience.
Include them after requiring FactoryGirl:

	require 'brainzz/factories'


Deployment
----------

This project makes use of branches to manage deployment.
Pushing a new commit to the `production` branch
will also build and push this gem to RubyGems.


[atv-wiki-google-api-key]: https://www.github.com/awesomenesstv/wiki#google-api-key
[google-oauth-playground]: https://developers.google.com/oauthplayground/
