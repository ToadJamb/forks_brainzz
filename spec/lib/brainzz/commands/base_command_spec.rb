require 'spec_helper'

RSpec.describe Brainzz::BaseCommand do
  describe 'base functionality' do
    subject do
      Class.new(described_class) do
        class << self
          def connection_value
            connection
          end

          def header_value
            headers
          end

          def base_url
            '/base-url'
          end
        end
      end
    end

    describe 'connection' do
      it 'is a connection object' do
        expect(subject.connection_value).to be_a Faraday::Connection
      end

      it 'uses base_url' do
        expect(subject.connection_value.build_url.to_s).to eq '/base-url'
      end
    end

    describe 'header values' do
      it 'adds content-type header' do
        expect(subject.header_value).to have_key 'Content-Type'
        expect(subject.header_value['Content-Type']).to eq 'application/json'
      end
    end
  end

  describe 'method overrides' do
    subject do
      Class.new(described_class) do
        class << self
          def base_url_value
            base_url
          end

          def endpoint_value
            endpoint
          end
        end
      end
    end

    describe '.base_url' do
      it 'raises an exception' do
        expect{ subject.base_url_value }.to raise_error NameError, /base_url/
      end
    end

    describe '.endpoint' do
      it 'raises an exception' do
        expect{ subject.endpoint_value }.to raise_error NameError, /endpoint/
      end
    end
  end
end
