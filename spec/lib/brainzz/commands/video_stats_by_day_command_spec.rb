require 'spec_helper'

RSpec.describe Brainzz::VideoStatsByDayCommand do

  let(:klass) do
    Class.new(Brainzz::ViewTotalsCommand) do
      extend Brainzz::VideoStatsByDayCommand
      class << self
        public :params
      end
    end
  end

  let(:start_date) { '2015-08-12' }
  let(:end_date) { '2015-08-18' }

  let(:input_params) {{
    :video_id   => 'video-id',
    :start_date => DateTime.parse(start_date),
    :end_date   => DateTime.parse(end_date),
  }}

  let(:youtube_params) {{
    'filters'   => 'video==video-id',
    'start-date' => start_date,
    'end-date'   => end_date,
  }}

  let(:video_stats_params) do
    Brainzz::VideoStatsParams.new(input_params)
  end

  describe 'params' do
    let(:by_day_params) {{
      'dimensions' => 'day',
      'sort' => 'day'
    }}

    it 'merges the by_day_params with additional params' do
      expect(klass.params video_stats_params).to include(by_day_params)
      expect(klass.params video_stats_params).to include(youtube_params)
    end
  end
end
