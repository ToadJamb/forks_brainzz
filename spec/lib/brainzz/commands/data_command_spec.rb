require 'spec_helper'

RSpec.describe Brainzz::DataCommand do
  subject do
    Class.new(described_class) do
      class << self
        def connection_value
          connection
        end

        def param_value
          params
        end
      end
    end
  end

  it "inherits from #{Brainzz::BaseCommand}" do
    expect(described_class.new).to be_a Brainzz::BaseCommand
  end

  describe 'base_url' do
    it 'uses the youtube v3 data api' do
      expect(subject.connection_value.build_url.to_s)
        .to match(/googleapis.*\/youtube\/v3/)
    end
  end

  describe 'parameter values' do
    before { stub_const 'ENV', {'BRAINZZ_GOOGLE_API_KEY' => 'google-api-key'} }

    it 'adds the api key' do
      expect(subject.param_value).to have_key :key
      expect(subject.param_value[:key]).to eq 'google-api-key'
    end
  end
end
