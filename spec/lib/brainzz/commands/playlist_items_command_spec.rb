require 'spec_helper'

RSpec.describe Brainzz::PlaylistItemsCommand do
  describe '.execute' do
    subject { described_class.execute playlist_id, previous }

    let(:playlist_id) { 'UUWljxewHlJE3M7U_6_zFNyA' }
    let(:connection) { instance_double Faraday::Connection }
    let(:response) { instance_double Faraday::Response }
    let(:params) {{
      :key         => 'google-api-key',
      'playlistId' => playlist_id,
      'part'       => 'contentDetails',
      'maxResults' => 50,
    }}
    let(:headers) {{ 'Content-Type' => 'application/json' }}

    it "inherits from #{Brainzz::DataCommand}" do
      expect(described_class.new).to be_a Brainzz::DataCommand
    end

    context 'given valid paramters' do
      before do
        allow(Faraday).to receive(:new).and_return connection
        allow(Brainzz::PlaylistItemsResponse).to receive(:new).with response
        stub_const 'ENV', {'BRAINZZ_GOOGLE_API_KEY' => 'google-api-key'}
      end

      context 'given no previous response' do
        let(:previous) { nil }

        it 'makes a request using the correct parameters' do
          expect(connection)
            .to receive(:get)
            .with('playlistItems', params, headers)
            .and_return response

          subject
        end
      end

      context 'given a previous response' do
        let(:previous) { instance_double Brainzz::PlaylistItemsResponse }

        let(:playlist_wrapper) do
          Brainzz::PlaylistItemsWrapper.new({
            'nextPageToken' => next_page_token,
            'items' => []
          })
        end

        let(:next_page_token) { 'next-page-token-value' }

        let(:params) {{
          :key         => 'google-api-key',
          'playlistId' => playlist_id,
          'part'       => 'contentDetails',
          'maxResults' => 50,
          'pageToken'  => 'next-page-token-value',
        }}

        before do
          allow(previous).to receive(:data).and_return playlist_wrapper
        end

        it 'makes a request using the correct parameters' do
          expect(connection)
            .to receive(:get)
            .with('playlistItems', params, headers)
            .and_return response

          subject
        end
      end
    end

    context 'given invalid parameters' do
      let(:playlist_id) { nil }
      let(:previous)    { nil }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Brainzz::PlaylistItemsResponse
        expect(subject.success?).to eq false
        expect(subject.status).to eq nil
        expect(subject.data).to eq nil
      end
    end
  end
end
