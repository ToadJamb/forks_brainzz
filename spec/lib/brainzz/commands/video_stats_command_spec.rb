require 'spec_helper'

RSpec.describe Brainzz::VideoStatsCommand do
  it "inherits from #{Brainzz::AnalyticsCommand}" do
    expect(described_class.new).to be_a Brainzz::AnalyticsCommand
  end

  let(:stat_count_class) { double 'StatCountResponse' }
  let(:klass) do
    Class.new(described_class) do
      class << self
        public :response
        public :make_with_response
      end
    end
  end

  describe '.response' do
    subject { klass }
    it 'gets the response' do
      subject.instance_variable_set :@response, stat_count_class
      expect(subject.response).to eq stat_count_class
    end
  end

  describe '.make_with_response' do
    subject { klass }
    it 'sets the response' do
      subject.make_with_response  stat_count_class
      expect(subject.instance_variable_get :@response).to eq stat_count_class
    end
  end

  describe '.execute' do
    subject { described_class.execute(video_id, start_date, end_date) }

    let(:video_id)   { generate :brainzz_video_id }
    let(:start_date) { DateTime.parse start_date_string }
    let(:end_date)   { DateTime.parse end_date_string }

    let(:start_date_string) { '2015-02-07' }
    let(:end_date_string)   { '2015-03-07' }

    let(:response)   { instance_double Faraday::Response }

    let(:video_stats_params) do
      Brainzz::VideoStatsParams.new({
        :video_id   => video_id,
        :start_date => start_date,
        :end_date   => end_date,
      })
    end

    before do
      allow(described_class).to receive(:get).and_return response
      allow(described_class).to receive(:response).and_return stat_count_class
      allow(Brainzz::VideoStatsParams)
        .to receive(:new)
        .and_return video_stats_params
    end

    context 'given valid params' do
      it 'creates a new response object with get response' do
        expect(described_class).to receive(:get).with video_stats_params
        expect(stat_count_class).to receive(:new).with response

        subject
      end
    end

    context 'given invalid params' do
      let(:video_id) { nil }

      it 'creates an empty response object' do
        expect(stat_count_class).to receive(:new)

        subject
      end
    end
  end
end
