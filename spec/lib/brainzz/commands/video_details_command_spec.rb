require 'spec_helper'

RSpec.describe Brainzz::VideoDetailsCommand do
  describe '.execute' do
    subject { described_class.execute video_ids }

    let(:video_ids)  {[
      generate(:brainzz_video_id),
      generate(:brainzz_video_id),
    ]}

    let(:connection) { instance_double Faraday::Connection }
    let(:response)   { instance_double Faraday::Response }
    let(:headers)    {{ 'Content-Type' => 'application/json' }}

    let(:params) {{
      'id'   => video_ids.join(','),
      'part' => 'snippet',
      :key   => 'google-api-key'
    }}

    it "inherits from #{Brainzz::DataCommand}" do
      expect(described_class.new).to be_a Brainzz::DataCommand
    end

    context 'given valid paramters' do
      before do
        allow(Faraday).to receive(:new).and_return connection
        allow(Brainzz::VideoDetailsResponse).to receive(:new).with response

        stub_const 'ENV', {'BRAINZZ_GOOGLE_API_KEY' => 'google-api-key'}
      end

      it 'makes a request using the correct parameters' do
        expect(connection)
          .to receive(:get)
          .with('videos', params, headers)
          .and_return response

        subject
      end
    end

    context 'given invalid parameters' do
      let(:video_ids) { nil }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Brainzz::VideoDetailsResponse
        expect(subject.success?).to eq false
        expect(subject.status).to eq nil
        expect(subject.data).to eq nil
      end
    end
  end
end
