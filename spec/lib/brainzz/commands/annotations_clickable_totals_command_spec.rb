require 'spec_helper'

RSpec.describe Brainzz::AnnotationsClickableTotalsCommand do
   describe '.execute' do
    subject { described_class.execute video_id, start_date, end_date }

    let(:connection) { instance_double Faraday::Connection }
    let(:response)   { instance_double Faraday::Response }

    let(:access_token)  { generate :toke_token }

    let(:headers)    {{
      'Content-Type'  => 'application/json',
      'Authorization' => "Bearer #{access_token}",
    }}

    let(:content_owner) { 'spec-content-owner' }

    let(:video_id)      { generate :brainzz_video_id }
    let(:start_date)    { DateTime.parse start_date_string }
    let(:end_date)      { DateTime.parse end_date_string }

    let(:start_date_string) { '2015-02-07' }
    let(:end_date_string)   { '2015-03-07' }

    let(:params) {{
      'ids'        => "contentOwner==#{content_owner}",
      'filters'    => "video==#{video_id}",
      'metrics'    => 'annotationClickableImpressions',
      'start-date' => start_date_string,
      'end-date'   => end_date_string,
    }}

    it "inherits from #{Brainzz::VideoStatsCommand}" do
      expect(described_class.new).to be_a Brainzz::VideoStatsCommand
    end

    context 'given valid paramters' do
      before do
        stub_const 'ENV', { 'BRAINZZ_CONTENT_OWNER' => content_owner }
        allow(Faraday).to receive(:new).and_return connection
        allow(Brainzz::AnnotationsClickableResponse).to receive(:new)
          .with response
        allow(Brainzz::AccessTokenService)
          .to receive(:retrieve_token)
          .and_return access_token
      end

      it 'makes a request using the correct parameters' do
        expect(connection)
          .to receive(:get)
          .with('reports', params, headers)
          .and_return response

        subject
      end
    end

    context 'given invalid parameters' do
      let(:video_id) { nil }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Brainzz::AnnotationsClickableResponse
        expect(subject.success?).to eq false
        expect(subject.status).to eq nil
        expect(subject.data).to eq nil
      end
    end
   end
end
