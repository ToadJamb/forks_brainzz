require 'spec_helper'

RSpec.describe Brainzz::AnalyticsCommand do
  subject do
    Class.new(described_class) do
      class << self
        def headers_value
          headers
        end

        def connection_value
          connection
        end
      end
    end
  end

  it "inherits from #{Brainzz::BaseCommand}" do
    expect(described_class.new).to be_a Brainzz::BaseCommand
  end

  describe 'base_url' do
    it 'uses the youtube v3 data api' do
      expect(subject.connection_value.build_url.to_s)
        .to match(/googleapis.*\/youtube\/analytics\/v1/)
    end
  end

  describe 'headers' do
    let(:access_token) { generate :toke_token }
    let(:params) do
      Brainzz::AnalyticsParams.new({
        :channel_id    => 'channel-id',
        :start_date    => DateTime.new(2015, 03, 01),
        :end_date      => DateTime.new(2015, 03, 10),
      })
    end

    before do
      stub_const 'ENV', {'BRAINZZ_REFRESH_TOKEN' => 'spec-refresh-token'}
      allow(Brainzz::AccessTokenService)
        .to receive(:retrieve_token)
        .with('spec-refresh-token')
        .and_return access_token
    end

    it 'adds an authorized access_token to the headers' do
      expect(subject.headers_value).to have_key 'Authorization'
      expect(subject.headers_value['Authorization'])
        .to eq "Bearer #{access_token}"
    end
  end
end
