require 'spec_helper'

RSpec.describe Brainzz::VideoDetailsResponse do
  describe '.new' do
    subject { described_class.new response }

    let(:response) { instance_double Faraday::Response }

    let(:hash) {{
      'kind' => 'youtube#videoListResponse',
      'items' => [{
        'kind' => 'youtube#video',
        'id' => 'lS7oI7HCDGA',
        'snippet' => {
          'publishedAt' => '2015-02-09T21:00:00.000Z',
          'title'       => 'TAYLOR SWIFT!'
        }
      }, {
        'kind' => 'youtube#video',
        'id' => '4jURY6thYJ8',
        'snippet' => {
          'publishedAt' => '2015-02-15T23:00:00.000Z',
          'title' => 'SELENA GOMEZ'
        }
      }]
    }}

    before { allow(response).to receive(:body).and_return hash.to_json }
    before { allow(response).to receive(:status).and_return 200}
    before { allow(response).to receive(:success?).and_return true}

    it 'sets the video details' do
      expect(subject.data).to be_a Hash
      expect(subject.data.length).to eq 2

      subject.data.each do |key, video_detail|
        expect(video_detail).to be_a Brainzz::Video
      end
    end
  end
end
