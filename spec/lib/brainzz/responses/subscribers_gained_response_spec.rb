require 'spec_helper'

RSpec.describe Brainzz::SubscribersGainedResponse do
  let(:body) {{}}

  it "inherits from #{Brainzz::VideoStatsResponse}" do
    expect(subject).to be_a Brainzz::VideoStatsResponse
  end

  describe '.on_success' do
    subject { described_class.new response }

    let(:response) { instance_double Faraday::Response }

    let(:subscribers_gained)  { 108.0 }

    before do
      allow(response).to receive(:body).and_return(body.to_json)
      allow(response).to receive(:status).and_return(200)
      allow(response).to receive(:success?).and_return(true)
    end

    context 'when subscribers gained totals are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'subscribersGained',
          },
        ],
        'rows' => [[
          subscribers_gained,
        ]]
      }}

      it 'creates a Brainzz::SubscribersGained object with appropriate data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::SubscribersGainedCount
        expect(subject.data[0].subscribers_gained).to eq 108
        expect(subject.data[0].day).to be_nil
      end
    end

    context 'when subscribers gained per day are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'subscribersGained',
          }, {
            'name' => 'day',
          },
        ],
        'rows' => [[
          subscribers_gained,
          date_string,
        ]]
      }}

      let(:date_string) { '2015-03-07' }
      let(:date) { Time.parse date_string }

      it 'creates a Brainzz::SubscribersGained object with appropriate data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::SubscribersGainedCount
        expect(subject.data[0].subscribers_gained).to eq 108
        expect(subject.data[0].day).to eq date
      end
    end
  end
end
