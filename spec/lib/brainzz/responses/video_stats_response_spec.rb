require 'spec_helper'

RSpec.describe Brainzz::VideoStatsResponse do
  describe '.model' do
    subject { described_class }

    it 'gets the model appropriately' do
      subject.instance_variable_set :@model, stat_count_class
      expect(subject.model).to eq stat_count_class
    end
  end

  let(:klass) do
    Class.new(described_class) do
      class << self
        public :make_with_model
      end
    end
  end

  let(:stat_count_class) { double 'StatCountModel' }

  describe '.make_with_model' do
    subject { klass }

    it 'sets the model appropriately' do
      subject.make_with_model stat_count_class
      expect(subject.instance_variable_get :@model).to eq stat_count_class
    end
  end

  describe '#on_success' do
    subject { klass.new response }

    let(:response) { instance_double Faraday::Response }
    let(:stat)     { 108.0 }

    before do
      klass.make_with_model stat_count_class
      allow(response).to receive(:body).and_return(body.to_json)
      allow(response).to receive(:status).and_return(200)
      allow(response).to receive(:success?).and_return(true)
    end

    context 'given only metric stats are returned' do
      let(:body) {{
        'columnHeaders': [{
           'name': 'stats',
        }],
        'rows': [[
           stat,
        ]]
      }}

      it 'passes data keys to the initializer of the model appropriately' do
        expect(stat_count_class).to receive(:new).with({
          :data => [stat],
          :keys => {'stats' => 0}
        })

        subject
      end
    end

    context 'given metrics and dimensions' do
      let(:date_string) { '2015-03-07' }

      context 'when a stat and a date are returned' do
        let(:body) {{
          'columnHeaders': [{
              'name': 'stats',
            },{
              'name': 'day',
            }],
          'rows': [[
            stat,
            date_string,
          ]]
        }}

        it 'passes data keys to the initializer of the model appropriately' do
          expect(stat_count_class).to receive(:new).with({
            :data => [stat, date_string],
            :keys => {'stats' => 0, 'day' => 1}
          })

          subject
        end
      end

      context 'when a stat, date, and source type are returned' do
        let(:source_type) { 'SUBSCRIBER' }

        let(:body) {{
          'columnHeaders': [{
            'name': 'day',
          },{
            'name': 'stats',
          },{
            'name': 'insightTrafficSourceType',
          }],
          'rows': [[
            date_string,
            stat,
            source_type,
          ]]
        }}

        it 'passes data keys to the initializer of the model appropriately' do
          expect(stat_count_class).to receive(:new).with({
            :data => [date_string, stat, source_type],
            :keys => {
              'day'                      => 0,
              'stats'                    => 1,
              'insightTrafficSourceType' => 2,
            }
          })

          subject
        end
      end

      context 'when no rows are returned on body' do
        let(:body) { {} }

        it 'sets data to an empty array' do
          expect(subject.data).to eq []
        end
      end
    end
  end
end
