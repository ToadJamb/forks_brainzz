require 'spec_helper'

RSpec.describe Brainzz::ViewPercentageResponse do
  let(:body) {{}}

  it "inherits from #{Brainzz::VideoStatsResponse}" do
    expect(subject).to be_a Brainzz::VideoStatsResponse
  end

  describe '.on_success' do
    subject { described_class.new response }

    let(:response) { instance_double Faraday::Response }

    let(:percentage)  { 12.345 }

    before do
      allow(response).to receive(:body).and_return(body.to_json)
      allow(response).to receive(:status).and_return(200)
      allow(response).to receive(:success?).and_return(true)
    end

    context 'when percentage totals are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'averageViewPercentage',
          },
        ],
        'rows' => [[
          percentage,
        ]]
      }}

      it 'creates a Brainzz::ViewPercentage object with good data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::ViewPercentage
        expect(subject.data[0].percentage).to eq 12.345
        expect(subject.data[0].day).to be_nil
      end
    end

    context 'when percentages per day are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'averageViewPercentage',
          }, {
            'name' => 'day',
          },
        ],
        'rows' => [[
          percentage,
          date_string,
        ]]
      }}

      let(:date_string) { '2015-03-07' }
      let(:date) { Time.parse date_string }

      it 'creates a Brainzz::ViewPercentage object with good data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::ViewPercentage
        expect(subject.data[0].percentage).to eq 12.345
        expect(subject.data[0].day).to eq date
      end
    end
  end
end
