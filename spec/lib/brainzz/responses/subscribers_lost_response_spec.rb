require 'spec_helper'

RSpec.describe Brainzz::SubscribersLostResponse do
  let(:body) {{}}

  it "inherits from #{Brainzz::VideoStatsResponse}" do
    expect(subject).to be_a Brainzz::VideoStatsResponse
  end

  describe '.on_success' do
    subject { described_class.new response }

    let(:response) { instance_double Faraday::Response }

    let(:subscribers_lost)  { 108.0 }

    before do
      allow(response).to receive(:body).and_return(body.to_json)
      allow(response).to receive(:status).and_return(200)
      allow(response).to receive(:success?).and_return(true)
    end

    context 'when subscribers lost totals are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'subscribersLost',
          },
        ],
        'rows' => [[
          subscribers_lost,
        ]]
      }}

      it 'creates a Brainzz::SubscribersLost object with appropriate data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::SubscribersLostCount
        expect(subject.data[0].subscribers_lost).to eq 108
        expect(subject.data[0].day).to be_nil
      end
    end

    context 'when subscribers lost per day are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'subscribersLost',
          }, {
            'name' => 'day',
          },
        ],
        'rows' => [[
          subscribers_lost,
          date_string,
        ]]
      }}

      let(:date_string) { '2015-03-07' }
      let(:date) { Time.parse date_string }

      it 'creates a Brainzz::SubscribersLost object with appropriate data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::SubscribersLostCount
        expect(subject.data[0].subscribers_lost).to eq 108
        expect(subject.data[0].day).to eq date
      end
    end
  end
end
