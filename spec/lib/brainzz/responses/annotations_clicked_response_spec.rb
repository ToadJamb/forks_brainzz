require 'spec_helper'

RSpec.describe Brainzz::AnnotationsClickedResponse do
  let(:body) {{}}

  it "inherits from #{Brainzz::VideoStatsResponse}" do
    expect(subject).to be_a Brainzz::VideoStatsResponse
  end

  describe '.on_success' do
    subject { described_class.new response }

    let(:response) { instance_double Faraday::Response }

    let(:annotations_clicked)  { 108.0 }

    before do
      allow(response).to receive(:body).and_return(body.to_json)
      allow(response).to receive(:status).and_return(200)
      allow(response).to receive(:success?).and_return(true)
    end

    context 'when annotation clicked totals are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'annotationClicks',
          },
        ],
        'rows' => [[
          annotations_clicked,
        ]]
      }}

      it 'creates a Brainzz::AnnotationClickedCount object with good data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::AnnotationsClickedCount
        expect(subject.data[0].annotations_clicked).to eq 108
        expect(subject.data[0].day).to be_nil
      end
    end

    context 'when annotation clicked counts per day are attained' do
      let(:body) {{
        'columnHeaders' => [{
            'name' => 'annotationClicks',
          }, {
            'name' => 'day',
          },
        ],
        'rows' => [[
          annotations_clicked,
          date_string,
        ]]
      }}

      let(:date_string) { '2015-03-07' }
      let(:date) { Time.parse date_string }

      it 'creates a Brainzz::AnnotationClickedCount object with good data' do
        expect(subject.data).to be_a ::Array
        expect(subject.data[0]).to be_a Brainzz::AnnotationsClickedCount
        expect(subject.data[0].annotations_clicked).to eq 108
        expect(subject.data[0].day).to eq date
      end
    end
  end
end
