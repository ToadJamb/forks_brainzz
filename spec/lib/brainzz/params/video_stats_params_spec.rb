require 'spec_helper'

RSpec.describe Brainzz::VideoStatsParams, :param do
  subject { described_class.new params }

  let(:params) {{
    :video_id      => video_id,
    :start_date    => start_date,
    :end_date      => end_date,
  }}

  let(:video_id) do
    defined?(@video_id) ? @video_id : generate(:brainzz_video_id)
  end

  let(:start_date) do
    defined?(@start_date) ? @start_date : DateTime.new(2015, 03, 01)
  end

  let(:end_date) do
    defined?(@end_date) ? @end_date : DateTime.new(2015, 03, 31)
  end

  it "inherits #{Brainzz::AnalyticsParams}" do
    expect(subject).to be_a Brainzz::AnalyticsParams
  end

  describe '.new' do
    it 'sets video_id' do
      expect(subject.video_id).to eq video_id
    end

    it_behaves_like 'a nil param attribute', :video_id, nil
    it_behaves_like 'a nil param attribute', :video_id, ''
    it_behaves_like 'a nil param attribute', :video_id, " \n "
  end

  describe '#valid?' do
    context 'given all attributes are set' do
      it 'returns true' do
        expect(subject.valid?).to eq true
      end
    end

    it_behaves_like 'an invalid param attribute', :video_id, nil
    it_behaves_like 'an invalid param attribute', :start_date, nil
    it_behaves_like 'an invalid param attribute', :end_date, nil
  end
end
