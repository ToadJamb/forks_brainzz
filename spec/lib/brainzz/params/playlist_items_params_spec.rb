require 'spec_helper'

RSpec.describe Brainzz::PlaylistItemsParams, :param do
  subject { described_class.new playlist_id, response }

  let(:playlist_id) { defined?(@playlist_id) ? @playlist_id : 'playlist-id' }

  let(:response) { build :reverb_response, :data => wrapper }
  let(:wrapper)  { build :brainzz_playlist_items_wrapper }

  describe '.new' do
    it 'sets playlist_id' do
      expect(subject.playlist_id).to eq 'playlist-id'
    end

    it 'sets next_page_token' do
      expect(subject.next_page_token).to eq wrapper.next_page_token
    end

    it_behaves_like 'a nil param attribute', :playlist_id, nil
    it_behaves_like 'a nil param attribute', :playlist_id, ''
    it_behaves_like 'a nil param attribute', :playlist_id, " \n "
  end

  describe '#valid?' do
    context 'given all attributes are set' do
      it 'returns true' do
        expect(subject.valid?).to eq true
      end
    end

    it_behaves_like 'an invalid param attribute', :playlist_id, nil
  end

  describe '#next_page_token' do
    context 'given no response is specified' do
      let(:response) { nil }
      it 'returns nil' do
        expect(subject.next_page_token).to eq nil
      end
    end

    context 'given no response data exists' do
      let(:wrapper) { nil }
      it 'returns nil' do
        expect(subject.next_page_token).to eq nil
      end
    end
  end
end
