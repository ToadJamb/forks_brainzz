require 'spec_helper'

RSpec.describe Brainzz::AnalyticsParams, :param do
  subject { described_class.new params }

  let(:params) {{
    :start_date    => start_date,
    :end_date      => end_date,
  }}

  let(:start_date) do
    defined?(@start_date) ? @start_date : DateTime.new(2015, 03, 01)
  end

  let(:end_date) do
    defined?(@end_date) ? @end_date : DateTime.new(2015, 03, 31)
  end

  describe '.new' do
    it 'sets start_date' do
      expect(subject.start_date).to eq '2015-03-01'
    end

    it 'sets end_date' do
      expect(subject.end_date).to eq '2015-03-31'
    end

    it_behaves_like 'a nil param attribute', :start_date, nil
    it_behaves_like 'a nil param attribute', :start_date, ''
    it_behaves_like 'a nil param attribute', :start_date, " \n "
    it_behaves_like 'a nil param attribute', :start_date, 'foo-bar'
    it_behaves_like 'a nil param attribute', :start_date, 12

    it_behaves_like 'a nil param attribute', :end_date, nil
    it_behaves_like 'a nil param attribute', :end_date, ''
    it_behaves_like 'a nil param attribute', :end_date, " \n "
    it_behaves_like 'a nil param attribute', :end_date, 'foo-bar'
    it_behaves_like 'a nil param attribute', :end_date, 12
  end

  describe '#valid?' do
    context 'given all attributes are set' do
      it 'returns true' do
        expect(subject.valid?).to eq true
      end
    end

    it_behaves_like 'an invalid param attribute', :start_date, nil
    it_behaves_like 'an invalid param attribute', :end_date, nil
  end
end
