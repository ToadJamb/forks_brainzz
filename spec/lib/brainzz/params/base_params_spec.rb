require 'spec_helper'

RSpec.describe Brainzz::BaseParams do
  let(:instance) do
    Class.new(described_class) do
      def normalize_value(value)
        normalize value
      end

      def normalize_date_value(value)
        normalize_date value
      end
    end.new
  end

  shared_examples 'a normalized value' do |input, output|
    context "given #{input.inspect}" do
      let(:value) { input }
      it "returns #{output.inspect}" do
        expect(subject).to eq output
      end
    end
  end

  describe '.normalize' do
    subject { instance.normalize_value value }

    it_behaves_like 'a normalized value', 'foo-bar', 'foo-bar'
    it_behaves_like 'a normalized value', nil, nil
    it_behaves_like 'a normalized value', '', nil
    it_behaves_like 'a normalized value', ' ', nil
    it_behaves_like 'a normalized value', " \n ", nil
  end

  describe '.normalize_date' do
    subject { instance.normalize_date_value value }

    it_behaves_like 'a normalized value',
      DateTime.parse('2015-03-18'), '2015-03-18'

    it_behaves_like 'a normalized value', Date.parse('2015-03-18'), '2015-03-18'
    it_behaves_like 'a normalized value', Time.parse('2015-03-18'), '2015-03-18'
    it_behaves_like 'a normalized value', 'foo-bar', nil
  end
end
