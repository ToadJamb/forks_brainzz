require 'spec_helper'

RSpec.describe Brainzz::VideoDetailsParams, :param do
  subject { described_class.new video_ids }

  let(:video_list) {[
    generate(:brainzz_video_id),
    generate(:brainzz_video_id),
  ]}

  let(:video_ids) { defined?(@video_ids) ? @video_ids : video_list }

  describe '.new' do
    it 'sets video_ids' do
      expect(subject.video_ids).to eq video_ids
    end
  end

  describe '#valid?' do
    context 'given all attributes are set' do
      it 'returns true' do
        expect(subject.valid?).to eq true
      end
    end

    it_behaves_like 'an invalid param attribute', :video_ids, nil
    it_behaves_like 'an invalid param attribute', :video_ids, []
  end
end
