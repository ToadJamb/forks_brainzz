require 'spec_helper'

RSpec.describe "#{Brainzz::SubscribersLostCount} factory", :factory do
  let(:factory) { :brainzz_subscribers_lost_count }

  it_behaves_like 'a provided factory',
    :brainzz_subscribers_lost_count, :subscribers_lost, Fixnum
  it_behaves_like 'a provided factory',
    :brainzz_subscribers_lost_count, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_subscribers_lost_count, :day, nil

  it_behaves_like 'a factory attribute', :subscribers_lost, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
