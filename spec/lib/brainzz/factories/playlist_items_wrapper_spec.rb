require 'spec_helper'

RSpec.describe "#{Brainzz::PlaylistItemsWrapper} factory", :factory do
  let(:factory) { :brainzz_playlist_items_wrapper }

  it_behaves_like 'a provided factory',
    :brainzz_playlist_items_wrapper, :next_page_token, /page-token-\d+/
  it_behaves_like 'a provided factory',
    :brainzz_playlist_items_wrapper, :playlist_items, Array

  it_behaves_like 'a factory attribute', :next_page_token, 'page-token'
  it_behaves_like 'a factory attribute', :playlist_items, ['item-1', 'item-2']

  describe '.playlist_items' do
    subject { build(factory).playlist_items }

    context 'by default' do
      it 'includes playlist items with videos' do
        expect(subject.count).to be > 1
        subject.each do |playlist_item|
          expect(playlist_item).to be_a Brainzz::PlaylistItem
          expect(playlist_item.id).to match(/playlist-item-id-\d+/)
          expect(playlist_item.video).to be_a Brainzz::Video
          expect(playlist_item.video.id).to match(/video-id-\d+/)
        end
      end
    end
  end
end
