require 'spec_helper'

RSpec.describe "#{Brainzz::DislikeCount} factory", :factory do
  let(:factory) { :brainzz_dislike_count }

  it_behaves_like 'a provided factory',
    :brainzz_dislike_count, :dislikes, Fixnum
  it_behaves_like 'a provided factory', :brainzz_dislike_count, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_dislike_count, :day, nil

  it_behaves_like 'a factory attribute', :dislikes, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
