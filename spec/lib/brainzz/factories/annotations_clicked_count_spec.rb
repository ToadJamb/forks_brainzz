require 'spec_helper'

RSpec.describe "#{Brainzz::AnnotationsClickedCount} factory", :factory do
  let(:factory) { :brainzz_annotations_clicked_count }

  it_behaves_like 'a provided factory',
    :brainzz_annotations_clicked_count,
    :annotations_clicked, Fixnum
  it_behaves_like 'a provided factory',
    :brainzz_annotations_clicked_count, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_annotations_clicked_count, :day, nil

  it_behaves_like 'a factory attribute', :annotations_clicked, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
