require 'spec_helper'

RSpec.describe "#{Brainzz::ViewDuration} factory", :factory do
  let(:factory) { :brainzz_view_duration }

  it_behaves_like 'a provided factory',
    :brainzz_view_duration, :duration, Fixnum
  it_behaves_like 'a provided factory', :brainzz_view_duration, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_view_duration, :day, nil

  it_behaves_like 'a factory attribute', :duration, 12.3456789
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
