require 'spec_helper'

RSpec.describe "#{Brainzz::ShareCount} factory", :factory do
  let(:factory) { :brainzz_share_count }

  it_behaves_like 'a provided factory', :brainzz_share_count, :shares, Fixnum
  it_behaves_like 'a provided factory', :brainzz_share_count, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_share_count, :day, nil

  it_behaves_like 'a factory attribute', :shares, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
