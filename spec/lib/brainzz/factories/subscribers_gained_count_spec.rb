require 'spec_helper'

RSpec.describe "#{Brainzz::SubscribersGainedCount} factory", :factory do
  let(:factory) { :brainzz_subscribers_gained_count }

  it_behaves_like 'a provided factory',
    :brainzz_subscribers_gained_count, :subscribers_gained, Fixnum
  it_behaves_like 'a provided factory',
    :brainzz_subscribers_gained_count, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_subscribers_gained_count, :day, nil

  it_behaves_like 'a factory attribute', :subscribers_gained, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
