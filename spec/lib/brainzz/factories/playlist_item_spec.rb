require 'spec_helper'

RSpec.describe "#{Brainzz::PlaylistItem} factory", :factory do
  let(:factory) { :brainzz_playlist_item }

  it_behaves_like 'a provided factory',
    :brainzz_playlist_item, :id, /playlist-item-id-\d+/
  it_behaves_like 'a provided factory',
    :brainzz_playlist_item, :video, Brainzz::Video

  it_behaves_like 'a factory attribute', :id, 'video-id'
  it_behaves_like 'a factory attribute', :video, 'video-item'
end

