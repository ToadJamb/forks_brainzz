require 'spec_helper'

RSpec.describe "#{Brainzz::CommentCount} factory", :factory do
  let(:factory) { :brainzz_comment_count }

  it_behaves_like 'a provided factory',
    :brainzz_comment_count, :comments, Fixnum
  it_behaves_like 'a provided factory', :brainzz_comment_count, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_comment_count, :day, nil

  it_behaves_like 'a factory attribute', :comments, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
