require 'spec_helper'

RSpec.describe "#{Brainzz::ViewCount} factory", :factory, :view_source do
  let(:factory) { :brainzz_view_count }

  it_behaves_like 'a provided factory', :brainzz_view_count, :views, Fixnum
  it_behaves_like 'a provided factory', :brainzz_view_count, :day, Time
  it_behaves_like 'a provided factory', :brainzz_view_count, :source, Fixnum

  it_behaves_like 'a provided factory', :brainzz_aggregate_view_count, :day, nil

  it_behaves_like 'a provided factory', :brainzz_unknown_view_count, :source

  it_behaves_like 'a provided factory',
    :brainzz_unknown_aggregate_view_count, :day, nil
  it_behaves_like 'a provided factory',
    :brainzz_unknown_aggregate_view_count, :source, -1

  SpecSupport::VIEW_SOURCES.each_with_index do |view_source, i|
    it_behaves_like 'a provided factory',
      "brainzz_#{view_source}_view_count", :source, i + 1
  end

  SpecSupport::VIEW_SOURCES.each_with_index do |view_source, i|
    it_behaves_like 'a provided factory',
      "brainzz_#{view_source}_aggregate_view_count", :source, i + 1
    it_behaves_like 'a provided factory',
      "brainzz_#{view_source}_aggregate_view_count", :day, nil
  end

  it_behaves_like 'a factory attribute', :views, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
  it_behaves_like 'a factory attribute', :source, 8

  describe '.source' do
    let(:view_counts) { build_list :brainzz_view_count, view_sources.count }

    it 'cycles through valid values' do
      expect(view_counts.map(&:source))
        .to match_array Brainzz::ViewSourceEnum::SOURCES.values
      expect(build(:brainzz_view_count).source).to eq view_counts.first.source
    end
  end
end
