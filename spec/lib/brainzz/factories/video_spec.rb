require 'spec_helper'

RSpec.describe "#{Brainzz::Video} factory", :factory do
  let(:factory) { :brainzz_video }

  it_behaves_like 'a provided factory', :brainzz_video, :id, /video-id-\d+/
  it_behaves_like 'a provided factory', :brainzz_video, :title, nil
  it_behaves_like 'a provided factory', :brainzz_video, :published_at, nil

  it_behaves_like 'a provided factory',
    :brainzz_published_video, :id, /video-id-\d+/
  it_behaves_like 'a provided factory',
    :brainzz_published_video, :title, /video-title-\w+/
  it_behaves_like 'a provided factory',
    :brainzz_published_video, :published_at, Time

  it_behaves_like 'a factory attribute', :id, 'video-id'
  it_behaves_like 'a factory attribute', :title, 'video-title'
  it_behaves_like 'a factory attribute', :published_at, 'published-at'
end
