require 'spec_helper'

RSpec.describe "#{Brainzz::AnnotationsClickableCount} factory", :factory do
  let(:factory) { :brainzz_annotations_clickable_count }

  it_behaves_like 'a provided factory',
    :brainzz_annotations_clickable_count,
    :annotations_clickable, Fixnum
  it_behaves_like 'a provided factory',
    :brainzz_annotations_clickable_count, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_annotations_clickable_count, :day, nil

  it_behaves_like 'a factory attribute', :annotations_clickable, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
