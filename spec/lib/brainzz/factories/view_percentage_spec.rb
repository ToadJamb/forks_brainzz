require 'spec_helper'

RSpec.describe "#{Brainzz::ViewPercentage} factory", :factory do
  let(:factory) { :brainzz_view_percentage }

  it_behaves_like 'a provided factory',
    :brainzz_view_percentage, :percentage, Float
  it_behaves_like 'a provided factory', :brainzz_view_percentage, :day, Time

  it_behaves_like 'a provided factory',
    :brainzz_aggregate_view_percentage, :day, nil

  it_behaves_like 'a factory attribute', :percentage, 12.3456789
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
