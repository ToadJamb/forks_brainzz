require 'spec_helper'

RSpec.describe "#{Brainzz::LikeCount} factory", :factory do
  let(:factory) { :brainzz_like_count }

  it_behaves_like 'a provided factory', :brainzz_like_count, :likes, Fixnum
  it_behaves_like 'a provided factory', :brainzz_like_count, :day, Time

  it_behaves_like 'a provided factory', :brainzz_aggregate_like_count, :day, nil

  it_behaves_like 'a factory attribute', :likes, 23
  it_behaves_like 'a factory attribute', :day, '2015-03-23'
end
