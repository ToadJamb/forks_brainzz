require 'spec_helper'

RSpec.describe Brainzz::ViewSourceEnum, :view_source do
  describe '::SOURCES' do
    it 'contains all the traffic source types' do
      expect(Brainzz::ViewSourceEnum::SOURCES.keys).to match_array view_sources
    end
  end

  shared_examples 'a traffic source type constant' do |constant, value|
    describe "#{described_class}::#{constant.upcase}" do
      subject { described_class.const_get constant.upcase }

      it "returns #{value.inspect}" do
        expect(subject).to eq value
      end
    end
  end

  it_behaves_like 'a traffic source type constant', 'advertising',       1
  it_behaves_like 'a traffic source type constant', 'annotation',        2
  it_behaves_like 'a traffic source type constant', 'ext_url',           3
  it_behaves_like 'a traffic source type constant', 'no_link_embedded',  4
  it_behaves_like 'a traffic source type constant', 'no_link_other',     5
  it_behaves_like 'a traffic source type constant', 'playlist',          6
  it_behaves_like 'a traffic source type constant', 'promoted',          7
  it_behaves_like 'a traffic source type constant', 'related_video',     8
  it_behaves_like 'a traffic source type constant', 'subscriber',        9
  it_behaves_like 'a traffic source type constant', 'yt_channel',       10
  it_behaves_like 'a traffic source type constant', 'yt_other_page',    11
  it_behaves_like 'a traffic source type constant', 'yt_search',        12

  describe '.key_for' do
    shared_examples 'traffic source key retriever' do |value, constant|
      subject { described_class.key_for value }

      context "given #{value.inspect}" do
        it "returns #{constant}" do
          expect(subject).to eq constant
        end
      end
    end

    it_behaves_like 'traffic source key retriever',  1, 'advertising'
    it_behaves_like 'traffic source key retriever',  2, 'annotation'
    it_behaves_like 'traffic source key retriever',  3, 'ext_url'
    it_behaves_like 'traffic source key retriever',  4, 'no_link_embedded'
    it_behaves_like 'traffic source key retriever',  5, 'no_link_other'
    it_behaves_like 'traffic source key retriever',  6, 'playlist'
    it_behaves_like 'traffic source key retriever',  7, 'promoted'
    it_behaves_like 'traffic source key retriever',  8, 'related_video'
    it_behaves_like 'traffic source key retriever',  9, 'subscriber'
    it_behaves_like 'traffic source key retriever', 10, 'yt_channel'
    it_behaves_like 'traffic source key retriever', 11, 'yt_other_page'
    it_behaves_like 'traffic source key retriever', 12, 'yt_search'
    it_behaves_like 'traffic source key retriever', -1, 'unknown'
    it_behaves_like 'traffic source key retriever',  0, nil
  end
end
