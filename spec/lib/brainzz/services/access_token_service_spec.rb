require 'spec_helper'

RSpec.describe Brainzz::AccessTokenService do
  let(:refresh_token) { 'refresh-token' }

  let(:params) {{
    :refresh_token => refresh_token,
    :client_id     => ENV['BRAINZZ_CLIENT_ID'],
    :client_secret => ENV['BRAINZZ_CLIENT_SECRET'],
  }}

  let(:access_token) { build :toke_access_token, :token => 'token-foo' }
  let(:response) { build :reverb_response, :data => access_token }

  describe '.retrieve_token' do
    subject { described_class.retrieve_token refresh_token }

    before { described_class.instance_variable_set :@token, nil }

    context 'given a token does not exist' do
      before { described_class.send :remove_instance_variable, :@token }

      it 'returns a token' do
        expect(Toke)
          .to receive(:retrieve_token)
          .with(params)
          .and_return response
        expect(subject).to eq('token-foo')
      end

      context 'given the response is not successful' do
        let(:response) { build :reverb_unsuccessful_response }

        it 'fails' do
          expect(Toke)
            .to receive(:retrieve_token)
            .and_return response

          expect(subject).to eq nil
        end
      end
    end

    context 'given a token exists' do
      before { described_class.instance_variable_set :@token, access_token }

      context 'given the token is not expired' do
        before { expect(access_token.expired?).to eq false }

        it 'returns the existing token' do
          expect(subject).to eq 'token-foo'
        end
      end

      context 'given the token is expired' do
        let(:expired_token) { build :toke_expired_access_token }

        before { described_class.instance_variable_set :@token, expired_token }

        before { expect(expired_token.expired?).to eq true }

        it 'returns a token' do
          expect(Toke)
            .to receive(:retrieve_token)
            .with(params)
            .and_return response
          expect(subject).to eq('token-foo')
        end
      end
    end
  end
end
