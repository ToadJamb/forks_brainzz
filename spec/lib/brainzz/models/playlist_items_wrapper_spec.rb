require 'spec_helper'

RSpec.describe Brainzz::PlaylistItemsWrapper do
  describe '.new' do
    subject { described_class.new data }
    let(:raw_videos) {[{
        'kind' => 'youtube#playlistItem',
        'etag' => 'etag-value',
        'id'   => 'id-value-1',
        'contentDetails' => {
          'videoId' => 'video-id-1',
        },
      }, {
        'kind' => 'youtube#playlistItem',
        'etag' => 'etag-value',
        'id'   => 'id-value-2',
        'contentDetails' => {
          'videoId' => 'video-id-2',
      },
    }]}

    let(:data) {{
      'kind'          => 'youtube#playlistItem',
      'etag'          => 'etag-value',
      'nextPageToken' => 'page-token',

      'pageInfo' => {
        'totalResults'   => 4116,
        'resultsPerPage' => 50,
      },

      'items' => raw_videos,
    }}

    it 'sets the playlist items' do
      expect(subject.playlist_items).to be_an Array
      expect(subject.playlist_items.length).to eq 2

      subject.playlist_items.each do |playlist_item|
        expect(playlist_item).to be_a Brainzz::PlaylistItem
      end
    end

    it 'sets the next page token' do
      expect(subject.next_page_token).to eq 'page-token'
    end
  end
end
