require 'spec_helper'

RSpec.describe Brainzz::ViewCount, :model do
  let(:hash) {{ :data => [], :keys => {} }}
  it "inherits from #{Brainzz::VideoStat}" do
    expect(described_class.new hash).to be_a Brainzz::VideoStat
  end

  describe '.new' do
    context 'given a hash of keys and view count data with dates' do
      it_behaves_like 'attribute setter for', :views, 1000, {
        :data => [
          '2015-03-09',
          'SUBSCRIBER',
          1000.3,
        ],
        :keys => {
          'day' => 0,
          'insightTrafficSourceType' => 1,
          'views' => 2,
        },
      }

      it_behaves_like 'attribute setter for', :views, nil, {
        :data => [
          '2015-03-09',
        ],
        :keys => {
          'day' => 0,
        }
      }

      it_behaves_like 'attribute setter for', :day,
        Time.parse('2015-03-09'), {
          :data => [
            '2015-03-09',
            'SUBSCRIBER',
            1000.0,
          ],
          :keys => {
            'day' => 0,
            'insightTrafficSourceType' => 1,
            'views' => 2,
          },
        }

      it_behaves_like 'attribute setter for', :source, 9, {
        :data => [
          '2015-03-09',
          'SUBSCRIBER',
          1000.0,
        ],
        :keys => {
          'day' => 0,
          'insightTrafficSourceType' => 1,
          'views' => 2,
        },
      }

      it_behaves_like 'attribute setter for', :source, 9, {
        :data => [
          'subscriber',
          '2015-03-09',
          1000.0,
        ],
        :keys => {
          'insightTrafficSourceType' => 0,
          'day' => 1,
          'views' => 2,
        },
      }

      it_behaves_like 'attribute setter for', :source, nil, {
        :data => [
          1000.0,
          'FOO',
          '2015-03-09',
        ],
        :keys => {
          'insightTrafficSourceType' => 1,
          'day' => 2,
          'views' => 0,
        },
      }
    end

    context 'given a hash of keys and view count data without dates' do

      it_behaves_like 'attribute setter for', :views, 1000, {
        :data => [
          'subscriber',
          1000.0,
        ],
        :keys => {
          'insightTrafficSourceType' => 0,
          'views' => 1,
        },
      }

      it_behaves_like 'attribute setter for', :source, 9, {
        :data => [
          'subscriber',
          1000.0,
        ],
        :keys => {
          'insightTrafficSourceType' => 0,
          'views' => 1,
        },
      }

      it_behaves_like 'attribute setter for', :day, nil, {
        :data => [
          'subscriber',
          1000.0,
        ],
        :keys => {
          'insightTrafficSourceType' => 0,
          'views' => 1,
        },
      }
    end

    context 'given a hash of keys and view count data without source' do

      it_behaves_like 'attribute setter for', :views, 1000, {
        :data => [
          '2015-03-07',
          1000.0,
        ],
        :keys => {
          'day' => 0,
          'views' => 1,
        },
      }

      it_behaves_like 'attribute setter for', :day,
        Time.parse('2015-03-07'), {
        :data => [
          '2015-03-07',
          1000.0,
        ],
        :keys => {
          'day' => 0,
          'views' => 1,
        },
      }

      it_behaves_like 'attribute setter for', :source, nil, {
        :data => [
          '2015-03-07',
          1000.0,
        ],
        :keys => {
          'day' => 0,
          'views' => 1,
        },
      }
    end
  end
end
