require 'spec_helper'

RSpec.describe Brainzz::PlaylistItem, :model do
  describe '.new' do
    subject { described_class.new hash }

    let(:hash) {{
      'kind' => 'youtube#playlistItem',
      'etag' => 'etag-value',
      'id'   => 'id-value',
      'contentDetails' => {
        'videoId' => 'video-id-value',
      },
    }}

    it_behaves_like 'attribute setter for', :id, 'id-value', {
      'id' => 'id-value',
    }

    it 'sets the video attribute' do
      expect(subject.video).to be_a Brainzz::Video
      expect(subject.video.id).to eq 'video-id-value'
    end
  end
end
