require 'spec_helper'

RSpec.describe Brainzz::AnnotationsClickableCount, :model do
  let(:hash) {{ :data => [], :keys => {} }}
  it "inherits from #{Brainzz::VideoStat}" do
    expect(described_class.new hash).to be_a Brainzz::VideoStat
  end

  describe '.new' do
    context 'given a hash of keys and annotation count data with dates' do

      it_behaves_like 'attribute setter for',
        :annotations_clickable, 1000, {
        :data => [
          '2015-03-09',
          1000.3,
        ],
        :keys => {
          'day' => 0,
          'annotationClickableImpressions' => 1,
        },
      }

      it_behaves_like 'attribute setter for',
        :annotations_clickable, nil, {
        :data => [
          '2015-03-09',
        ],
        :keys => {
          'day' => 0,
        }
      }

      it_behaves_like 'attribute setter for', :day,
        Time.parse('2015-03-09'), {
          :data => [
            '2015-03-09',
            1000.0,
          ],
          :keys => {
            'day' => 0,
            'annotationClickableImpressions' => 1,
          },
        }
    end

    context 'given a hash of keys and annotation count data without dates' do

      it_behaves_like 'attribute setter for',
        :annotations_clickable, 1000, {
        :data => [
          1000.0,
        ],
        :keys => {
          'annotationClickableImpressions' => 0,
        },
      }
    end
  end
end
