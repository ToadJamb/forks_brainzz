require 'spec_helper'

RSpec.describe Brainzz::ViewDuration, :model do
  let(:hash) {{ :data => [], :keys => {} }}
  it "inherits from #{Brainzz::VideoStat}" do
    expect(described_class.new hash).to be_a Brainzz::VideoStat
  end

  describe '.new' do
    context 'given a hash of keys and view duration data with dates' do

      it_behaves_like 'attribute setter for', :duration, 300, {
        :data => [
          '2015-03-09',
          300.0,
        ],
        :keys => {
          'day' => 0,
          'averageViewDuration' => 1,
        },
      }

      it_behaves_like 'attribute setter for', :duration, nil, {
        :data => [
          '2015-03-09',
        ],
        :keys => {
          'day' => 0,
        }
      }

      it_behaves_like 'attribute setter for', :day,
        Time.parse('2015-03-09'), {
          :data => [
            '2015-03-09',
            300.0,
          ],
          :keys => {
            'day' => 0,
            'averageViewDuration' => 1,
          },
        }
    end

    context 'given a hash of keys and view duration data without dates' do

      it_behaves_like 'attribute setter for', :duration, 300, {
        :data => [
          300.0,
        ],
        :keys => {
          'averageViewDuration' => 0,
        },
      }
    end
  end
end
