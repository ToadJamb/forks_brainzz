require 'spec_helper'

RSpec.describe Brainzz::BaseModel do
  let(:klass) do
    Class.new(described_class) do
      def initialize(value)
        @value = value
      end

      def transformed_value
        transform @value
      end

      def string_transformed_value
        transform_string @value
      end

      def date_transformed_value
        transform_date @value
      end
    end
  end

  shared_examples 'a transformed value' do |input, output|
    let(:object) { klass.new value }

    let(:value) { input }

    describe '.transform' do
      subject { object.transformed_value }

      context "given #{input.inspect}" do
        it "returns #{output.inspect}" do
          expect(subject).to eq output
        end
      end
    end
  end

  shared_examples 'a date transformed value' do |input, output|
    let(:object) { klass.new value }

    let(:value) { input }

    describe '.transform_date' do
      subject { object.date_transformed_value }

      context "given #{input.inspect}" do
        it "returns #{output.inspect}" do
          expect(subject).to eq output
        end
      end
    end
  end

  it_behaves_like 'a transformed value', 'foo', 'foo'
  it_behaves_like 'a transformed value', nil, nil
  it_behaves_like 'a transformed value', 1234, 1234
  it_behaves_like 'a transformed value', '', nil
  it_behaves_like 'a transformed value', ' ', nil
  it_behaves_like 'a transformed value', " \n ", nil

  it_behaves_like 'a date transformed value',
    '2015-04-12', Time.parse('2015-04-12')
  it_behaves_like 'a date transformed value', nil, nil
  it_behaves_like 'a date transformed value', '', nil
  it_behaves_like 'a date transformed value', ' ', nil
  it_behaves_like 'a date transformed value', " \n ", nil

  describe '.transform_date' do
    subject { klass.new('2015-04-23').date_transformed_value }

    it "returns a #{Time}" do
      expect(subject).to be_a Time
    end
  end
end
