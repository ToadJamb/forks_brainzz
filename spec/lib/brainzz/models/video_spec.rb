require 'spec_helper'

RSpec.describe Brainzz::Video, :model do
  describe '.new' do
    context 'given a playlist item hash' do
      it_behaves_like 'attribute setter for', :id, 'id-value', {
        'videoId' => 'id-value',
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'videoId' => nil,
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'videoId' => '',
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'videoId' => ' ',
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'videoId' => " \n ",
      }
    end

    context 'given a video details hash' do
      it_behaves_like 'attribute setter for', :id, 'id-value', {
        'id' => 'id-value',
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'id' => nil,
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'id' => '',
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'id' => ' ',
      }
      it_behaves_like 'attribute setter for', :id, nil, {
        'id' => " \n ",
      }

      it_behaves_like 'attribute setter for', :title, 'TAYLOR SWIFT!', {
        'snippet' => { 'title' => 'TAYLOR SWIFT!' },
      }
      it_behaves_like 'attribute setter for', :title, nil, {
        'snippet' => { 'title' => nil },
      }
      it_behaves_like 'attribute setter for', :title, nil, {
        'snippet' => { 'title' => '' },
      }
      it_behaves_like 'attribute setter for', :title, nil, {
        'snippet' => { 'title' => ' ' },
      }
      it_behaves_like 'attribute setter for', :title, nil, {
        'snippet' => { 'title' => " \n " },
      }

      it_behaves_like 'attribute setter for',
        :published_at, Time.parse('2015-02-09T21:00:00.000Z'), {
        'snippet' => { 'publishedAt' => '2015-02-09T21:00:00.000Z' },
      }
      it_behaves_like 'attribute setter for', :published_at, nil, {
        'snippet' => { 'publishedAt' => '' },
      }
      it_behaves_like 'attribute setter for', :published_at, nil, {
        'snippet' => { 'publishedAt' => ' ' },
      }
      it_behaves_like 'attribute setter for', :published_at, nil, {
        'snippet' => { 'publishedAt' => " \n " },
      }
    end
  end
end
