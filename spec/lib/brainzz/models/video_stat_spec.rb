require 'spec_helper'

RSpec.describe Brainzz::VideoStat, :model do
  describe '.new' do
    context 'given a hash of keys and view count data with dates' do

      it_behaves_like 'attribute setter for', :day,
        Time.parse('2015-03-09'), {
          :data => [
            '2015-03-09',
            1000.0,
          ],
          :keys => {
            'day' => 0,
            'likes' => 1,
          },
        }
    end
  end
end
