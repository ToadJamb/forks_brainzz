require 'spec_helper'

RSpec.describe Brainzz::ViewPercentage, :model do
  let(:hash) {{ :data => [], :keys => {} }}
  it "inherits from #{Brainzz::VideoStat}" do
    expect(described_class.new hash).to be_a Brainzz::VideoStat
  end

  describe '.new' do
    context 'given a hash of keys and view percentage data with dates' do

      it_behaves_like 'attribute setter for', :percentage, 12.345, {
        :data => [
          '2015-03-09',
          12.345,
        ],
        :keys => {
          'day' => 0,
          'averageViewPercentage' => 1,
        },
      }

      it_behaves_like 'attribute setter for', :percentage, nil, {
        :data => [
          '2015-03-09',
        ],
        :keys => {
          'day' => 0,
        }
      }

      it_behaves_like 'attribute setter for', :day,
        Time.parse('2015-03-09'), {
          :data => [
            '2015-03-09',
            12.345,
          ],
          :keys => {
            'day' => 0,
            'averageViewPercentage' => 1,
          },
        }
    end

    context 'given a hash of keys and view percentage data without dates' do

      it_behaves_like 'attribute setter for', :percentage, 12.345, {
        :data => [
          12.345,
        ],
        :keys => {
          'averageViewPercentage' => 0,
        },
      }
    end
  end
end
