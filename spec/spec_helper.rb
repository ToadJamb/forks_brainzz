require_relative '../lib/brainzz'

Dir['spec/support/**/*.rb'].each do |file|
  require File.expand_path(file)
end

require_relative '../lib/brainzz/factories'

Dir['spec/shared/**/*.rb'].each do |file|
  require File.expand_path(file)
end

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.backtrace_exclusion_patterns << %r|/users/.*/\.rvm/gems/|i
  config.backtrace_exclusion_patterns << %r|/vendor/bundle/gems/|i

  config.disable_monkey_patching!
  config.warnings = true

  config.order = :random

  Kernel.srand config.seed
end
