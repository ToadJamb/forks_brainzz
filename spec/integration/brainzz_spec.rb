require 'integration_helper'

RSpec.describe Brainzz, :vcr do
  before do
    Brainzz::AccessTokenService.instance_variable_set :@token, nil
    Brainzz::AccessTokenService.send :remove_instance_variable, :@token
  end

  describe '.video_details' do
    subject { described_class.video_details_for video_ids }

    let(:video_ids) { ['lS7oI7HCDGA', '4jURY6thYJ8']}

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response
      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_a Hash

      expect(subject.data.keys).to match_array video_ids

      subject.data.each do |key, video_details|
        expect(video_details).to be_a Brainzz::Video
      end
    end

    context 'given more than 50 videos' do
      let(:video_ids) { ['lS7oI7HCDGA'] * 51 }

      before { expect(video_ids.count).to be > 50 }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Reverb::Response
        expect(subject.status).to eq 400
        expect(subject.success?).to eq false
        expect(subject.data).to eq nil
      end
    end
  end

  describe '.playlist_items_for' do
    subject { described_class.playlist_items_for playlist_id }

    let(:playlist_id) { 'UUWljxewHlJE3M7U_6_zFNyA' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response
      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_a Brainzz::PlaylistItemsWrapper
      expect(subject.data.playlist_items.count).to eq 50

      subject.data.playlist_items.each do |item|
        expect(item).to be_a Brainzz::PlaylistItem
      end

      expect(subject.data.last_page?).to eq false
    end

    context 'given a response object' do
      subject { described_class.playlist_items_for playlist_id, response }

      let(:response) { described_class.playlist_items_for playlist_id }
      let(:video_ids) { subject.data.playlist_items.map(&:video).map(&:id) }

      it 'returns the next page' do
        expect(subject).to be_a Reverb::Response
        expect(subject.status).to eq 200
        expect(subject.success?).to eq true

        expect(subject.data).to be_a Brainzz::PlaylistItemsWrapper
        expect(subject.data.playlist_items.count).to eq 50

        subject.data.playlist_items.each do |item|
          expect(item).to be_a Brainzz::PlaylistItem
        end

        response.data.playlist_items.each do |playlist_item|
          expect(video_ids).to_not include playlist_item.video.id
        end
      end

      context 'given the last page is requested' do
        let(:playlist_id) { 'UUWPvuKNudjOighT76v5rdTg' }

        before do
          @subject = described_class.playlist_items_for playlist_id
          while @subject.data.next_page_token do
            @subject = described_class.playlist_items_for playlist_id, response
          end
        end

        it 'indicates that it is the last page' do
          expect(@subject).to be_a Reverb::Response
          expect(@subject.status).to eq 200
          expect(@subject.success?).to eq true

          expect(@subject.data.last_page?).to eq true
        end
      end
    end

    context 'given the request fails' do
      before { @api_key = ENV['BRAINZZ_GOOGLE_API_KEY'] }
      before { ENV['BRAINZZ_GOOGLE_API_KEY'] = 'fake-api-key' }
      after { ENV['BRAINZZ_GOOGLE_API_KEY'] = @api_key }

      it 'returns an unsuccessful response' do
        expect(subject).to be_a Reverb::Response
        expect(subject.status).to eq 400
        expect(subject.success?).to eq false

        expect(subject.data).to eq nil
      end
    end
  end

  describe '.views_by_day_for' do
    subject { described_class.views_by_day_for video_id, start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |view_count|
        expect(view_count).to be_a Brainzz::ViewCount
        expect(view_count.day).to be >= current
        current = view_count.day
      end
    end
  end

  describe '.view_totals_for' do
    subject { described_class.view_totals_for video_id, start_date, end_date }

    let(:video_id)   { 'Y4hP-AKj9Lo' }
    let(:start_date) { DateTime.parse '2015-02-02' }
    let(:end_date)   { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |view_count|
        expect(view_count).to be_a Brainzz::ViewCount
        expect(view_count.day).to be_nil
      end
    end
  end

  describe '.likes_by_day_for' do
    subject { described_class.likes_by_day_for video_id, start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |like_count|
        expect(like_count).to be_a Brainzz::LikeCount
        expect(like_count.day).to be >= current
        current = like_count.day
      end
    end
  end

  describe '.like_totals_for' do
    subject { described_class.like_totals_for video_id, start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |like_count|
        expect(like_count).to be_a Brainzz::LikeCount
        expect(like_count.day).to be_nil
      end
    end
  end

  describe '.shares_by_day_for' do
    subject { described_class.shares_by_day_for video_id, start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |share_count|
        expect(share_count).to be_a Brainzz::ShareCount
        expect(share_count.day).to be >= current
        current = share_count.day
      end
    end
  end

  describe '.share_totals_for' do
    subject { described_class.share_totals_for video_id, start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |share_count|
        expect(share_count).to be_a Brainzz::ShareCount
        expect(share_count.day).to be_nil
      end
    end
  end

  describe '.dislikes_by_day_for' do
    subject { described_class.dislikes_by_day_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |dislike_count|
        expect(dislike_count).to be_a Brainzz::DislikeCount
        expect(dislike_count.day).to be >= current
        current = dislike_count.day
      end
    end
  end

  describe '.dislike_totals_for' do
    subject { described_class.dislike_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |dislike_count|
        expect(dislike_count).to be_a Brainzz::DislikeCount
        expect(dislike_count.day).to be_nil
      end
    end
  end

  describe '.subscribers_gained_by_day_for' do
    subject { described_class.subscribers_gained_by_day_for  video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |subscribers_count|
        expect(subscribers_count).to be_a Brainzz::SubscribersGainedCount
        expect(subscribers_count.day).to be >= current
        current = subscribers_count.day
      end
    end
  end

  describe '.subscribers_gained_totals_for' do
    subject { described_class.subscribers_gained_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |subscribers_count|
        expect(subscribers_count).to be_a Brainzz::SubscribersGainedCount
        expect(subscribers_count.day).to be_nil
      end
    end
  end

  describe '.subscribers_lost_by_day_for' do
    subject { described_class.subscribers_lost_by_day_for  video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |subscribers_count|
        expect(subscribers_count).to be_a Brainzz::SubscribersLostCount
        expect(subscribers_count.day).to be >= current
        current = subscribers_count.day
      end
    end
  end

  describe '.subscribers_lost_totals_for' do
    subject { described_class.subscribers_lost_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |subscribers_count|
        expect(subscribers_count).to be_a Brainzz::SubscribersLostCount
        expect(subscribers_count.day).to be_nil
      end
    end
  end

  describe '.annotations_clickable_by_day_for' do
    subject { described_class.annotations_clickable_by_day_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |annotation_count|
        expect(annotation_count).to be_a Brainzz::AnnotationsClickableCount
        expect(annotation_count.day).to be >= current
        current = annotation_count.day
      end
    end
  end

  describe '.annotations_clickable_totals_for' do
    subject { described_class.annotations_clickable_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |annotation_count|
        expect(annotation_count).to be_a Brainzz::AnnotationsClickableCount
        expect(annotation_count.day).to be_nil
      end
    end
  end

  describe '.annotations_clicked_by_day_for' do
    subject { described_class.annotations_clicked_by_day_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |annotation_count|
        expect(annotation_count).to be_a Brainzz::AnnotationsClickedCount
        expect(annotation_count.day).to be >= current
        current = annotation_count.day
      end
    end
  end

  describe '.annotations_clicked_totals_for' do
    subject { described_class.annotations_clicked_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |annotation_count|
        expect(annotation_count).to be_a Brainzz::AnnotationsClickedCount
        expect(annotation_count.day).to be_nil
      end
    end
  end

  describe '.comments_by_day_for' do
    subject { described_class.comments_by_day_for  video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |comment_count|
        expect(comment_count).to be_a Brainzz::CommentCount
        expect(comment_count.day).to be >= current
        current = comment_count.day
      end
    end
  end

  describe '.comment_totals_for' do
    subject { described_class.comment_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |comment_count|
        expect(comment_count).to be_a Brainzz::CommentCount
        expect(comment_count.day).to be_nil
      end
    end
  end

  describe '.view_percentages_by_day_for' do
    subject { described_class.view_percentages_by_day_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |view_percentage|
        expect(view_percentage).to be_a Brainzz::ViewPercentage
        expect(view_percentage.day).to be >= current
        current = view_percentage.day
      end
    end
  end

  describe '.view_percentage_totals_for' do
    subject { described_class.view_percentage_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |view_percentage|
        expect(view_percentage).to be_a Brainzz::ViewPercentage
        expect(view_percentage.day).to be_nil
      end
    end
  end

  describe '.view_durations_by_day_for' do
    subject { described_class.view_durations_by_day_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { Time.parse '2015-02-02' }
    let(:end_date)      { Time.parse '2015-03-27' }

    it 'returns a response object with the resulting data sorted by date' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      current = start_date - 1
      subject.data.each do |view_duration|
        expect(view_duration).to be_a Brainzz::ViewDuration
        expect(view_duration.day).to be >= current
        current = view_duration.day
      end
    end
  end

  describe '.view_duration_totals_for' do
    subject { described_class.view_duration_totals_for video_id,
              start_date, end_date }

    let(:video_id)      { 'Y4hP-AKj9Lo' }
    let(:start_date)    { DateTime.parse '2015-02-02' }
    let(:end_date)      { DateTime.parse '2015-03-27' }

    it 'returns a response object with the resulting data' do
      expect(subject).to be_a Reverb::Response

      expect(subject.status).to eq 200
      expect(subject.success?).to eq true

      expect(subject.data).to be_an Array

      expect(subject.data.count).to be > 0

      subject.data.each do |view_duration|
        expect(view_duration).to be_a Brainzz::ViewDuration
        expect(view_duration.day).to be_nil
      end
    end
  end

  context 'given an invalid token for an analytics request' do
    subject { described_class.view_totals_for video_id, start_date, end_date }

    let(:video_id)   { 'Y4hP-AKj9Lo' }
    let(:start_date) { DateTime.parse '2015-02-02' }
    let(:end_date)   { DateTime.parse '2015-03-27' }

    let(:expired_token) { 'foo-bar' }

    before do
      allow(Brainzz::AccessTokenService)
        .to receive(:retrieve_token)
        .and_return expired_token
    end

    it 'returns an unsuccessful response' do
      expect(subject.status).to eq 401
      expect(subject.success?).to eq false
    end
  end
end
