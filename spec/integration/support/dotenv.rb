require 'dotenv'

[
  :integration,
  :test,
].each do |env|
  Dotenv.load "config/dotenv/#{env}.env"
end
