require 'vcr'

VCR.configure do |config|
  config.cassette_library_dir = 'spec/vcr_cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
  [
    'BRAINZZ_GOOGLE_API_KEY',
    'BRAINZZ_REFRESH_TOKEN',
    'BRAINZZ_CLIENT_ID',
    'BRAINZZ_CLIENT_SECRET',
    'BRAINZZ_CONTENT_OWNER',
  ].each do |env_var|
    config.filter_sensitive_data("<#{env_var}>") { ENV[env_var] }
  end
end
