module SpecSupport
  # These values are from the youtube api:
  # https://developers.google.com/youtube/analytics/v1/
  # dimsmets/dims#Traffic_Source_Dimensions
  VIEW_SOURCES = [
    'advertising',
    'annotation',
    'ext_url',
    'no_link_embedded',
    'no_link_other',
    'playlist',
    'promoted',
    'related_video',
    'subscriber',
    'yt_channel',
    'yt_other_page',
    'yt_search',
    'ext_app',
    'info_card',
  ]
end

RSpec.shared_context :view_source => true do
  let(:view_sources) { SpecSupport::VIEW_SOURCES }
end
