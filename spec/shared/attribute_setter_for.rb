RSpec.shared_context :model => true do
  shared_examples_for 'attribute setter for' do |attribute, output, hash|
    subject { described_class.new hash }
    context "given #{hash}" do
      it "sets #{attribute} to #{output.inspect}" do
        expect(subject.send(attribute)).to eq output
      end
    end
  end
end
