RSpec.shared_context :param => true do
  shared_examples 'an invalid param attribute' do |attribute, value|
    context "given #{attribute} is #{value.inspect}" do
      before { instance_variable_set "@#{attribute}", value }
      before { expect(subject.send(attribute)).to eq value }

      it 'returns false' do
        expect(subject.valid?).to eq false
      end
    end
  end
end
