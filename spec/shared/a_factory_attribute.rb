RSpec.shared_context :factory => true do
  shared_examples 'a factory attribute' do |attribute, value|
    subject { build factory, attribute => value }

    context "given #{attribute} is set to #{value.inspect}" do
      describe "#{attribute}" do
        it "returns #{value.inspect}" do
          expect(subject.send(attribute)).to eq value
        end
      end
    end
  end
end
