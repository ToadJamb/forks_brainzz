RSpec.shared_context :param => true do
  shared_examples 'a nil param attribute' do |attribute, value|
    before { instance_variable_set "@#{attribute}", value }
    describe "##{attribute}" do
      context "given #{value.inspect}" do
        it 'returns nil' do
          expect(subject.send(attribute)).to eq nil
        end
      end
    end
  end
end
